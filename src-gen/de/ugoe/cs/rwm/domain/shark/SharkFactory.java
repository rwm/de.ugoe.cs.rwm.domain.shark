/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage
 * @generated
 */
public interface SharkFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SharkFactory eINSTANCE = de.ugoe.cs.rwm.domain.shark.impl.SharkFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Sharkjob</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sharkjob</em>'.
	 * @generated
	 */
	Sharkjob createSharkjob();

	/**
	 * Returns a new object of class '<em>Vcsjob</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vcsjob</em>'.
	 * @generated
	 */
	Vcsjob createVcsjob();

	/**
	 * Returns a new object of class '<em>Mecojob</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mecojob</em>'.
	 * @generated
	 */
	Mecojob createMecojob();

	/**
	 * Returns a new object of class '<em>Coastjob</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Coastjob</em>'.
	 * @generated
	 */
	Coastjob createCoastjob();

	/**
	 * Returns a new object of class '<em>Memejob</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Memejob</em>'.
	 * @generated
	 */
	Memejob createMemejob();

	/**
	 * Returns a new object of class '<em>Databasedependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Databasedependency</em>'.
	 * @generated
	 */
	Databasedependency createDatabasedependency();

	/**
	 * Returns a new object of class '<em>Sharkjobrevision</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sharkjobrevision</em>'.
	 * @generated
	 */
	Sharkjobrevision createSharkjobrevision();

	/**
	 * Returns a new object of class '<em>Vcsshark</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vcsshark</em>'.
	 * @generated
	 */
	Vcsshark createVcsshark();

	/**
	 * Returns a new object of class '<em>Memeshark</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Memeshark</em>'.
	 * @generated
	 */
	Memeshark createMemeshark();

	/**
	 * Returns a new object of class '<em>Mecoshark</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mecoshark</em>'.
	 * @generated
	 */
	Mecoshark createMecoshark();

	/**
	 * Returns a new object of class '<em>Coastshark</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Coastshark</em>'.
	 * @generated
	 */
	Coastshark createCoastshark();

	/**
	 * Returns a new object of class '<em>Databasequery</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Databasequery</em>'.
	 * @generated
	 */
	Databasequery createDatabasequery();

	/**
	 * Returns a new object of class '<em>Mongoquery</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mongoquery</em>'.
	 * @generated
	 */
	Mongoquery createMongoquery();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SharkPackage getSharkPackage();

} //SharkFactory
