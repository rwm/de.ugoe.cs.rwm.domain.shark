/*******************************************************************************
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *************************************************************************
 * This code is 100% auto-generated
 * from:
 *   /de.ugoe.cs.rwm.domain.shark/model/shark.ecore
 * using:
 *   /de.ugoe.cs.rwm.domain.shark/model/shark.genmodel
 *   org.eclipse.ocl.examples.codegen.oclinecore.OCLinEcoreTables
 *
 * Do not edit it.
 *******************************************************************************/
package de.ugoe.cs.rwm.domain.shark;

import de.ugoe.cs.rwm.domain.shark.SharkTables;
import modmacao.ModmacaoTables;
import org.eclipse.cmf.occi.core.OCCITables;
import org.eclipse.ocl.pivot.ParameterTypes;
import org.eclipse.ocl.pivot.TemplateParameters;
import org.eclipse.ocl.pivot.ids.TypeId;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorPackage;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorProperty;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorType;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorFragment;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorOperation;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorProperty;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorStandardLibrary;
import org.eclipse.ocl.pivot.oclstdlib.OCLstdlibTables;
import org.eclipse.ocl.pivot.utilities.TypeUtil;

/**
 * SharkTables provides the dispatch tables for the shark for use by the OCL dispatcher.
 *
 * In order to ensure correct static initialization, a top level class element must be accessed
 * before any nested class element. Therefore an access to PACKAGE.getClass() is recommended.
 */
public class SharkTables
{
	static {
		Init.initStart();
	}

	/**
	 *	The package descriptor for the package.
	 */
	public static final /*@NonNull*/ EcoreExecutorPackage PACKAGE = new EcoreExecutorPackage(SharkPackage.eINSTANCE);

	/**
	 *	The library of all packages and types.
	 */
	public static final /*@NonNull*/ ExecutorStandardLibrary LIBRARY = OCLstdlibTables.LIBRARY;

	/**
	 *	Constants used by auto-generated code.
	 */
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.NsURIPackageId PACKid_http_c_s_s_schemas_modmacao_org_s_modmacao_s_ecore = org.eclipse.ocl.pivot.ids.IdManager.getNsURIPackageId("http://schemas.modmacao.org/modmacao/ecore", null, modmacao.ModmacaoPackage.eINSTANCE);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.NsURIPackageId PACKid_http_c_s_s_schemas_modmacao_org_s_occi_s_platform_s_ecore = org.eclipse.ocl.pivot.ids.IdManager.getNsURIPackageId("http://schemas.modmacao.org/occi/platform/ecore", null, org.modmacao.occi.platform.PlatformPackage.eINSTANCE);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.DataTypeId DATAid_Port = de.ugoe.cs.rwm.domain.shark.SharkTables.PACKid_http_c_s_s_schemas_modmacao_org_s_modmacao_s_ecore.getDataTypeId("Port", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.DataTypeId DATAid_URL = de.ugoe.cs.rwm.domain.shark.SharkTables.PACKid_http_c_s_s_schemas_modmacao_org_s_occi_s_platform_s_ecore.getDataTypeId("URL", 0);

	/**
	 *	The type parameters for templated types and operations.
	 */
	public static class TypeParameters {
		static {
			Init.initStart();
			SharkTables.init();
		}

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::TypeParameters and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The type descriptors for each type.
	 */
	public static class Types {
		static {
			Init.initStart();
			TypeParameters.init();
		}

		public static final /*@NonNull*/ EcoreExecutorType _Boolean = new EcoreExecutorType(TypeId.BOOLEAN, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Coastjob = new EcoreExecutorType(SharkPackage.Literals.COASTJOB, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Coastshark = new EcoreExecutorType(SharkPackage.Literals.COASTSHARK, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Databasedependency = new EcoreExecutorType(SharkPackage.Literals.DATABASEDEPENDENCY, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Databasequery = new EcoreExecutorType(SharkPackage.Literals.DATABASEQUERY, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Integer = new EcoreExecutorType(TypeId.INTEGER, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Mecojob = new EcoreExecutorType(SharkPackage.Literals.MECOJOB, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Mecoshark = new EcoreExecutorType(SharkPackage.Literals.MECOSHARK, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Memejob = new EcoreExecutorType(SharkPackage.Literals.MEMEJOB, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Memeshark = new EcoreExecutorType(SharkPackage.Literals.MEMESHARK, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Mongoquery = new EcoreExecutorType(SharkPackage.Literals.MONGOQUERY, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Sharkjob = new EcoreExecutorType(SharkPackage.Literals.SHARKJOB, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Sharkjobrevision = new EcoreExecutorType(SharkPackage.Literals.SHARKJOBREVISION, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _String = new EcoreExecutorType(TypeId.STRING, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Vcsjob = new EcoreExecutorType(SharkPackage.Literals.VCSJOB, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Vcsshark = new EcoreExecutorType(SharkPackage.Literals.VCSSHARK, PACKAGE, 0);

		private static final /*@NonNull*/ EcoreExecutorType /*@NonNull*/ [] types = {
			_Boolean,
			_Coastjob,
			_Coastshark,
			_Databasedependency,
			_Databasequery,
			_Integer,
			_Mecojob,
			_Mecoshark,
			_Memejob,
			_Memeshark,
			_Mongoquery,
			_Sharkjob,
			_Sharkjobrevision,
			_String,
			_Vcsjob,
			_Vcsshark
		};

		/*
		 *	Install the type descriptors in the package descriptor.
		 */
		static {
			PACKAGE.init(LIBRARY, types);
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::Types and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The fragment descriptors for the local elements of each type and its supertypes.
	 */
	public static class Fragments {
		static {
			Init.initStart();
			Types.init();
		}

		private static final /*@NonNull*/ ExecutorFragment _Boolean__Boolean = new ExecutorFragment(Types._Boolean, SharkTables.Types._Boolean);
		private static final /*@NonNull*/ ExecutorFragment _Boolean__OclAny = new ExecutorFragment(Types._Boolean, OCLstdlibTables.Types._OclAny);

		private static final /*@NonNull*/ ExecutorFragment _Coastjob__Coastjob = new ExecutorFragment(Types._Coastjob, SharkTables.Types._Coastjob);
		private static final /*@NonNull*/ ExecutorFragment _Coastjob__Component = new ExecutorFragment(Types._Coastjob, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Coastjob__MixinBase = new ExecutorFragment(Types._Coastjob, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Coastjob__OclAny = new ExecutorFragment(Types._Coastjob, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Coastjob__OclElement = new ExecutorFragment(Types._Coastjob, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Coastjob__Sharkjob = new ExecutorFragment(Types._Coastjob, SharkTables.Types._Sharkjob);
		private static final /*@NonNull*/ ExecutorFragment _Coastjob__Sharkjobrevision = new ExecutorFragment(Types._Coastjob, SharkTables.Types._Sharkjobrevision);

		private static final /*@NonNull*/ ExecutorFragment _Coastshark__Coastshark = new ExecutorFragment(Types._Coastshark, SharkTables.Types._Coastshark);
		private static final /*@NonNull*/ ExecutorFragment _Coastshark__Component = new ExecutorFragment(Types._Coastshark, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Coastshark__MixinBase = new ExecutorFragment(Types._Coastshark, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Coastshark__OclAny = new ExecutorFragment(Types._Coastshark, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Coastshark__OclElement = new ExecutorFragment(Types._Coastshark, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Databasedependency__Databasedependency = new ExecutorFragment(Types._Databasedependency, SharkTables.Types._Databasedependency);
		private static final /*@NonNull*/ ExecutorFragment _Databasedependency__Dependency = new ExecutorFragment(Types._Databasedependency, ModmacaoTables.Types._Dependency);
		private static final /*@NonNull*/ ExecutorFragment _Databasedependency__Executiondependency = new ExecutorFragment(Types._Databasedependency, ModmacaoTables.Types._Executiondependency);
		private static final /*@NonNull*/ ExecutorFragment _Databasedependency__MixinBase = new ExecutorFragment(Types._Databasedependency, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Databasedependency__OclAny = new ExecutorFragment(Types._Databasedependency, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Databasedependency__OclElement = new ExecutorFragment(Types._Databasedependency, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Databasequery__Component = new ExecutorFragment(Types._Databasequery, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Databasequery__Databasequery = new ExecutorFragment(Types._Databasequery, SharkTables.Types._Databasequery);
		private static final /*@NonNull*/ ExecutorFragment _Databasequery__MixinBase = new ExecutorFragment(Types._Databasequery, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Databasequery__OclAny = new ExecutorFragment(Types._Databasequery, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Databasequery__OclElement = new ExecutorFragment(Types._Databasequery, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Integer__Integer = new ExecutorFragment(Types._Integer, SharkTables.Types._Integer);
		private static final /*@NonNull*/ ExecutorFragment _Integer__OclAny = new ExecutorFragment(Types._Integer, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Integer__OclComparable = new ExecutorFragment(Types._Integer, OCLstdlibTables.Types._OclComparable);
		private static final /*@NonNull*/ ExecutorFragment _Integer__OclSummable = new ExecutorFragment(Types._Integer, OCLstdlibTables.Types._OclSummable);
		private static final /*@NonNull*/ ExecutorFragment _Integer__Real = new ExecutorFragment(Types._Integer, OCLstdlibTables.Types._Real);

		private static final /*@NonNull*/ ExecutorFragment _Mecojob__Component = new ExecutorFragment(Types._Mecojob, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Mecojob__Mecojob = new ExecutorFragment(Types._Mecojob, SharkTables.Types._Mecojob);
		private static final /*@NonNull*/ ExecutorFragment _Mecojob__MixinBase = new ExecutorFragment(Types._Mecojob, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Mecojob__OclAny = new ExecutorFragment(Types._Mecojob, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Mecojob__OclElement = new ExecutorFragment(Types._Mecojob, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Mecojob__Sharkjob = new ExecutorFragment(Types._Mecojob, SharkTables.Types._Sharkjob);
		private static final /*@NonNull*/ ExecutorFragment _Mecojob__Sharkjobrevision = new ExecutorFragment(Types._Mecojob, SharkTables.Types._Sharkjobrevision);

		private static final /*@NonNull*/ ExecutorFragment _Mecoshark__Component = new ExecutorFragment(Types._Mecoshark, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Mecoshark__Mecoshark = new ExecutorFragment(Types._Mecoshark, SharkTables.Types._Mecoshark);
		private static final /*@NonNull*/ ExecutorFragment _Mecoshark__MixinBase = new ExecutorFragment(Types._Mecoshark, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Mecoshark__OclAny = new ExecutorFragment(Types._Mecoshark, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Mecoshark__OclElement = new ExecutorFragment(Types._Mecoshark, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Memejob__Component = new ExecutorFragment(Types._Memejob, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Memejob__Memejob = new ExecutorFragment(Types._Memejob, SharkTables.Types._Memejob);
		private static final /*@NonNull*/ ExecutorFragment _Memejob__MixinBase = new ExecutorFragment(Types._Memejob, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Memejob__OclAny = new ExecutorFragment(Types._Memejob, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Memejob__OclElement = new ExecutorFragment(Types._Memejob, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Memejob__Sharkjob = new ExecutorFragment(Types._Memejob, SharkTables.Types._Sharkjob);

		private static final /*@NonNull*/ ExecutorFragment _Memeshark__Component = new ExecutorFragment(Types._Memeshark, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Memeshark__Memeshark = new ExecutorFragment(Types._Memeshark, SharkTables.Types._Memeshark);
		private static final /*@NonNull*/ ExecutorFragment _Memeshark__MixinBase = new ExecutorFragment(Types._Memeshark, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Memeshark__OclAny = new ExecutorFragment(Types._Memeshark, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Memeshark__OclElement = new ExecutorFragment(Types._Memeshark, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Mongoquery__Component = new ExecutorFragment(Types._Mongoquery, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Mongoquery__Databasequery = new ExecutorFragment(Types._Mongoquery, SharkTables.Types._Databasequery);
		private static final /*@NonNull*/ ExecutorFragment _Mongoquery__MixinBase = new ExecutorFragment(Types._Mongoquery, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Mongoquery__Mongoquery = new ExecutorFragment(Types._Mongoquery, SharkTables.Types._Mongoquery);
		private static final /*@NonNull*/ ExecutorFragment _Mongoquery__OclAny = new ExecutorFragment(Types._Mongoquery, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Mongoquery__OclElement = new ExecutorFragment(Types._Mongoquery, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Sharkjob__Component = new ExecutorFragment(Types._Sharkjob, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjob__MixinBase = new ExecutorFragment(Types._Sharkjob, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjob__OclAny = new ExecutorFragment(Types._Sharkjob, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjob__OclElement = new ExecutorFragment(Types._Sharkjob, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjob__Sharkjob = new ExecutorFragment(Types._Sharkjob, SharkTables.Types._Sharkjob);

		private static final /*@NonNull*/ ExecutorFragment _Sharkjobrevision__Component = new ExecutorFragment(Types._Sharkjobrevision, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjobrevision__MixinBase = new ExecutorFragment(Types._Sharkjobrevision, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjobrevision__OclAny = new ExecutorFragment(Types._Sharkjobrevision, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjobrevision__OclElement = new ExecutorFragment(Types._Sharkjobrevision, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjobrevision__Sharkjob = new ExecutorFragment(Types._Sharkjobrevision, SharkTables.Types._Sharkjob);
		private static final /*@NonNull*/ ExecutorFragment _Sharkjobrevision__Sharkjobrevision = new ExecutorFragment(Types._Sharkjobrevision, SharkTables.Types._Sharkjobrevision);

		private static final /*@NonNull*/ ExecutorFragment _String__OclAny = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _String__OclComparable = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclComparable);
		private static final /*@NonNull*/ ExecutorFragment _String__OclSummable = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclSummable);
		private static final /*@NonNull*/ ExecutorFragment _String__String = new ExecutorFragment(Types._String, SharkTables.Types._String);

		private static final /*@NonNull*/ ExecutorFragment _Vcsjob__Component = new ExecutorFragment(Types._Vcsjob, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Vcsjob__MixinBase = new ExecutorFragment(Types._Vcsjob, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Vcsjob__OclAny = new ExecutorFragment(Types._Vcsjob, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Vcsjob__OclElement = new ExecutorFragment(Types._Vcsjob, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Vcsjob__Sharkjob = new ExecutorFragment(Types._Vcsjob, SharkTables.Types._Sharkjob);
		private static final /*@NonNull*/ ExecutorFragment _Vcsjob__Vcsjob = new ExecutorFragment(Types._Vcsjob, SharkTables.Types._Vcsjob);

		private static final /*@NonNull*/ ExecutorFragment _Vcsshark__Component = new ExecutorFragment(Types._Vcsshark, ModmacaoTables.Types._Component);
		private static final /*@NonNull*/ ExecutorFragment _Vcsshark__MixinBase = new ExecutorFragment(Types._Vcsshark, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Vcsshark__OclAny = new ExecutorFragment(Types._Vcsshark, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Vcsshark__OclElement = new ExecutorFragment(Types._Vcsshark, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Vcsshark__Vcsshark = new ExecutorFragment(Types._Vcsshark, SharkTables.Types._Vcsshark);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::Fragments and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The parameter lists shared by operations.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Parameters {
		static {
			Init.initStart();
			Fragments.init();
		}

		public static final /*@NonNull*/ ParameterTypes _Boolean = TypeUtil.createParameterTypes(OCLstdlibTables.Types._Boolean);
		public static final /*@NonNull*/ ParameterTypes _Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._Integer);
		public static final /*@NonNull*/ ParameterTypes _Integer___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._Integer);
		public static final /*@NonNull*/ ParameterTypes _OclAny___OclAny___OclAny___Integer___Boolean___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._Boolean, OCLstdlibTables.Types._Integer);
		public static final /*@NonNull*/ ParameterTypes _OclAny___OclAny___OclAny___OclAny___String___Integer___OclAny___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._String, OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._Integer);
		public static final /*@NonNull*/ ParameterTypes _OclSelf = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclSelf);
		public static final /*@NonNull*/ ParameterTypes _String = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String);
		public static final /*@NonNull*/ ParameterTypes _String___Boolean = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String, OCLstdlibTables.Types._Boolean);
		public static final /*@NonNull*/ ParameterTypes _String___String = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String, OCLstdlibTables.Types._String);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::Parameters and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The operation descriptors for each operation of each type.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Operations {
		static {
			Init.initStart();
			Parameters.init();
		}

		public static final /*@NonNull*/ ExecutorOperation _Boolean___lt__gt_ = new ExecutorOperation("<>", Parameters._OclSelf, Types._Boolean,
			0, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyNotEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean___eq_ = new ExecutorOperation("=", Parameters._OclSelf, Types._Boolean,
			1, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__and = new ExecutorOperation("and", Parameters._Boolean, Types._Boolean,
			2, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanAndOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__implies = new ExecutorOperation("implies", Parameters._Boolean, Types._Boolean,
			3, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanImpliesOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__not = new ExecutorOperation("not", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Boolean,
			4, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanNotOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__or = new ExecutorOperation("or", Parameters._Boolean, Types._Boolean,
			5, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanOrOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__xor = new ExecutorOperation("xor", Parameters._Boolean, Types._Boolean,
			6, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanXorOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__allInstances = new ExecutorOperation("allInstances", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Boolean,
			7, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanAllInstancesOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__and2 = new ExecutorOperation("and2", Parameters._Boolean, Types._Boolean,
			8, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanAndOperation2.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__implies2 = new ExecutorOperation("implies2", Parameters._Boolean, Types._Boolean,
			9, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanImpliesOperation2.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__not2 = new ExecutorOperation("not2", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Boolean,
			10, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanNotOperation2.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__or2 = new ExecutorOperation("or2", Parameters._Boolean, Types._Boolean,
			11, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanOrOperation2.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__toString = new ExecutorOperation("toString", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Boolean,
			12, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyToStringOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__xor2 = new ExecutorOperation("xor2", Parameters._Boolean, Types._Boolean,
			13, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanXorOperation2.INSTANCE);

		public static final /*@NonNull*/ ExecutorOperation _Integer___mul_ = new ExecutorOperation("*", Parameters._OclSelf, Types._Integer,
			0, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericTimesOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer___add_ = new ExecutorOperation("+", Parameters._OclSelf, Types._Integer,
			1, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericPlusOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer___neg_ = new ExecutorOperation("-", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Integer,
			2, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericNegateOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer___sub_ = new ExecutorOperation("-", Parameters._OclSelf, Types._Integer,
			3, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericMinusOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer___div_ = new ExecutorOperation("/", Parameters._OclSelf, Types._Integer,
			4, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericDivideOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__abs = new ExecutorOperation("abs", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Integer,
			5, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericAbsOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__div = new ExecutorOperation("div", Parameters._Integer, Types._Integer,
			6, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericDivOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__max = new ExecutorOperation("max", Parameters._OclSelf, Types._Integer,
			7, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericMaxOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__min = new ExecutorOperation("min", Parameters._OclSelf, Types._Integer,
			8, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericMinOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__mod = new ExecutorOperation("mod", Parameters._Integer, Types._Integer,
			9, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericModOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__toString = new ExecutorOperation("toString", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Integer,
			10, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyToStringOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__toUnlimitedNatural = new ExecutorOperation("toUnlimitedNatural", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Integer,
			11, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.IntegerToUnlimitedNaturalOperation.INSTANCE);

		public static final /*@NonNull*/ ExecutorOperation _String___add_ = new ExecutorOperation("+", Parameters._String, Types._String,
			0, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringConcatOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___lt_ = new ExecutorOperation("<", Parameters._OclSelf, Types._String,
			1, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLessThanOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___lt__eq_ = new ExecutorOperation("<=", Parameters._OclSelf, Types._String,
			2, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLessThanEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___lt__gt_ = new ExecutorOperation("<>", Parameters._OclSelf, Types._String,
			3, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyNotEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___eq_ = new ExecutorOperation("=", Parameters._OclSelf, Types._String,
			4, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___gt_ = new ExecutorOperation(">", Parameters._OclSelf, Types._String,
			5, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringGreaterThanOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___gt__eq_ = new ExecutorOperation(">=", Parameters._OclSelf, Types._String,
			6, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringGreaterThanEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__at = new ExecutorOperation("at", Parameters._Integer, Types._String,
			7, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringAtOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__characters = new ExecutorOperation("characters", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			8, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringCharactersOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__compareTo = new ExecutorOperation("compareTo", Parameters._OclSelf, Types._String,
			9, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringCompareToOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__concat = new ExecutorOperation("concat", Parameters._String, Types._String,
			10, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringConcatOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__endsWith = new ExecutorOperation("endsWith", Parameters._String, Types._String,
			11, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringEndsWithOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__equalsIgnoreCase = new ExecutorOperation("equalsIgnoreCase", Parameters._String, Types._String,
			12, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringEqualsIgnoreCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__getSeverity = new ExecutorOperation("getSeverity", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			13, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringGetSeverityOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__indexOf = new ExecutorOperation("indexOf", Parameters._String, Types._String,
			14, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringIndexOfOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__lastIndexOf = new ExecutorOperation("lastIndexOf", Parameters._String, Types._String,
			15, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLastIndexOfOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__0_logDiagnostic = new ExecutorOperation("logDiagnostic", Parameters._OclAny___OclAny___OclAny___Integer___Boolean___Integer, Types._String,
			16, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__1_logDiagnostic = new ExecutorOperation("logDiagnostic", Parameters._OclAny___OclAny___OclAny___OclAny___String___Integer___OclAny___Integer, Types._String,
			17, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__matches = new ExecutorOperation("matches", Parameters._String, Types._String,
			18, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringMatchesOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__replaceAll = new ExecutorOperation("replaceAll", Parameters._String___String, Types._String,
			19, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringReplaceAllOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__replaceFirst = new ExecutorOperation("replaceFirst", Parameters._String___String, Types._String,
			20, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringReplaceFirstOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__size = new ExecutorOperation("size", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			21, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSizeOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__startsWith = new ExecutorOperation("startsWith", Parameters._String, Types._String,
			22, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringStartsWithOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__substituteAll = new ExecutorOperation("substituteAll", Parameters._String___String, Types._String,
			23, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstituteAllOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__substituteFirst = new ExecutorOperation("substituteFirst", Parameters._String___String, Types._String,
			24, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstituteFirstOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__substring = new ExecutorOperation("substring", Parameters._Integer___Integer, Types._String,
			25, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstringOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toBoolean = new ExecutorOperation("toBoolean", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			26, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToBooleanOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toInteger = new ExecutorOperation("toInteger", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			27, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToIntegerOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toLower = new ExecutorOperation("toLower", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			28, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToLowerCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toLowerCase = new ExecutorOperation("toLowerCase", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			29, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToLowerCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toReal = new ExecutorOperation("toReal", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			30, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToRealOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toString = new ExecutorOperation("toString", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			31, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyToStringOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toUpper = new ExecutorOperation("toUpper", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			32, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToUpperCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toUpperCase = new ExecutorOperation("toUpperCase", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			33, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToUpperCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__0_tokenize = new ExecutorOperation("tokenize", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			34, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__1_tokenize = new ExecutorOperation("tokenize", Parameters._String, Types._String,
			35, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__2_tokenize = new ExecutorOperation("tokenize", Parameters._String___Boolean, Types._String,
			36, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__trim = new ExecutorOperation("trim", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			37, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTrimOperation.INSTANCE);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::Operations and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The property descriptors for each property of each type.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Properties {
		static {
			Init.initStart();
			Operations.init();
		}


		public static final /*@NonNull*/ ExecutorProperty _Coastjob__coastMethodMetrics = new EcoreExecutorProperty(SharkPackage.Literals.COASTJOB__COAST_METHOD_METRICS, Types._Coastjob, 0);

		public static final /*@NonNull*/ ExecutorProperty _Databasedependency__databaseAuthentication = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEDEPENDENCY__DATABASE_AUTHENTICATION, Types._Databasedependency, 0);
		public static final /*@NonNull*/ ExecutorProperty _Databasedependency__databaseHostname = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEDEPENDENCY__DATABASE_HOSTNAME, Types._Databasedependency, 1);
		public static final /*@NonNull*/ ExecutorProperty _Databasedependency__databaseName = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEDEPENDENCY__DATABASE_NAME, Types._Databasedependency, 2);
		public static final /*@NonNull*/ ExecutorProperty _Databasedependency__databasePassword = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEDEPENDENCY__DATABASE_PASSWORD, Types._Databasedependency, 3);
		public static final /*@NonNull*/ ExecutorProperty _Databasedependency__databasePort = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEDEPENDENCY__DATABASE_PORT, Types._Databasedependency, 4);
		public static final /*@NonNull*/ ExecutorProperty _Databasedependency__databaseSslConnection = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION, Types._Databasedependency, 5);
		public static final /*@NonNull*/ ExecutorProperty _Databasedependency__databaseUser = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEDEPENDENCY__DATABASE_USER, Types._Databasedependency, 6);
		public static final /*@NonNull*/ ExecutorProperty _Databasedependency__dbDriver = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEDEPENDENCY__DB_DRIVER, Types._Databasedependency, 7);

		public static final /*@NonNull*/ ExecutorProperty _Databasequery__databaseName = new EcoreExecutorProperty(SharkPackage.Literals.DATABASEQUERY__DATABASE_NAME, Types._Databasequery, 0);

		public static final /*@NonNull*/ ExecutorProperty _Mecojob__mecoOutputDirectory = new EcoreExecutorProperty(SharkPackage.Literals.MECOJOB__MECO_OUTPUT_DIRECTORY, Types._Mecojob, 0);

		public static final /*@NonNull*/ ExecutorProperty _Memejob__memeParallelProcesses = new EcoreExecutorProperty(SharkPackage.Literals.MEMEJOB__MEME_PARALLEL_PROCESSES, Types._Memejob, 0);

		public static final /*@NonNull*/ ExecutorProperty _Mongoquery__mongoEvalJs = new EcoreExecutorProperty(SharkPackage.Literals.MONGOQUERY__MONGO_EVAL_JS, Types._Mongoquery, 0);

		public static final /*@NonNull*/ ExecutorProperty _Sharkjob__sharkLogLevel = new EcoreExecutorProperty(SharkPackage.Literals.SHARKJOB__SHARK_LOG_LEVEL, Types._Sharkjob, 0);
		public static final /*@NonNull*/ ExecutorProperty _Sharkjob__sharkProjectDirectory = new EcoreExecutorProperty(SharkPackage.Literals.SHARKJOB__SHARK_PROJECT_DIRECTORY, Types._Sharkjob, 1);
		public static final /*@NonNull*/ ExecutorProperty _Sharkjob__sharkProjectName = new EcoreExecutorProperty(SharkPackage.Literals.SHARKJOB__SHARK_PROJECT_NAME, Types._Sharkjob, 2);
		public static final /*@NonNull*/ ExecutorProperty _Sharkjob__sharkProjectRepository = new EcoreExecutorProperty(SharkPackage.Literals.SHARKJOB__SHARK_PROJECT_REPOSITORY, Types._Sharkjob, 3);

		public static final /*@NonNull*/ ExecutorProperty _Sharkjobrevision__sharkProjectRevision = new EcoreExecutorProperty(SharkPackage.Literals.SHARKJOBREVISION__SHARK_PROJECT_REVISION, Types._Sharkjobrevision, 0);

		public static final /*@NonNull*/ ExecutorProperty _Vcsjob__vcsUsedcores = new EcoreExecutorProperty(SharkPackage.Literals.VCSJOB__VCS_USEDCORES, Types._Vcsjob, 0);
		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::Properties and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The fragments for all base types in depth order: OclAny first, OclSelf last.
	 */
	public static class TypeFragments {
		static {
			Init.initStart();
			Properties.init();
		}

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Boolean =
			{
				Fragments._Boolean__OclAny /* 0 */,
				Fragments._Boolean__Boolean /* 1 */
			};
		private static final int /*@NonNull*/ [] __Boolean = { 1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Coastjob =
			{
				Fragments._Coastjob__OclAny /* 0 */,
				Fragments._Coastjob__OclElement /* 1 */,
				Fragments._Coastjob__MixinBase /* 2 */,
				Fragments._Coastjob__Component /* 3 */,
				Fragments._Coastjob__Sharkjob /* 4 */,
				Fragments._Coastjob__Sharkjobrevision /* 5 */,
				Fragments._Coastjob__Coastjob /* 6 */
			};
		private static final int /*@NonNull*/ [] __Coastjob = { 1,1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Coastshark =
			{
				Fragments._Coastshark__OclAny /* 0 */,
				Fragments._Coastshark__OclElement /* 1 */,
				Fragments._Coastshark__MixinBase /* 2 */,
				Fragments._Coastshark__Component /* 3 */,
				Fragments._Coastshark__Coastshark /* 4 */
			};
		private static final int /*@NonNull*/ [] __Coastshark = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Databasedependency =
			{
				Fragments._Databasedependency__OclAny /* 0 */,
				Fragments._Databasedependency__OclElement /* 1 */,
				Fragments._Databasedependency__MixinBase /* 2 */,
				Fragments._Databasedependency__Dependency /* 3 */,
				Fragments._Databasedependency__Executiondependency /* 4 */,
				Fragments._Databasedependency__Databasedependency /* 5 */
			};
		private static final int /*@NonNull*/ [] __Databasedependency = { 1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Databasequery =
			{
				Fragments._Databasequery__OclAny /* 0 */,
				Fragments._Databasequery__OclElement /* 1 */,
				Fragments._Databasequery__MixinBase /* 2 */,
				Fragments._Databasequery__Component /* 3 */,
				Fragments._Databasequery__Databasequery /* 4 */
			};
		private static final int /*@NonNull*/ [] __Databasequery = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Integer =
			{
				Fragments._Integer__OclAny /* 0 */,
				Fragments._Integer__OclComparable /* 1 */,
				Fragments._Integer__OclSummable /* 1 */,
				Fragments._Integer__Real /* 2 */,
				Fragments._Integer__Integer /* 3 */
			};
		private static final int /*@NonNull*/ [] __Integer = { 1,2,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Mecojob =
			{
				Fragments._Mecojob__OclAny /* 0 */,
				Fragments._Mecojob__OclElement /* 1 */,
				Fragments._Mecojob__MixinBase /* 2 */,
				Fragments._Mecojob__Component /* 3 */,
				Fragments._Mecojob__Sharkjob /* 4 */,
				Fragments._Mecojob__Sharkjobrevision /* 5 */,
				Fragments._Mecojob__Mecojob /* 6 */
			};
		private static final int /*@NonNull*/ [] __Mecojob = { 1,1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Mecoshark =
			{
				Fragments._Mecoshark__OclAny /* 0 */,
				Fragments._Mecoshark__OclElement /* 1 */,
				Fragments._Mecoshark__MixinBase /* 2 */,
				Fragments._Mecoshark__Component /* 3 */,
				Fragments._Mecoshark__Mecoshark /* 4 */
			};
		private static final int /*@NonNull*/ [] __Mecoshark = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Memejob =
			{
				Fragments._Memejob__OclAny /* 0 */,
				Fragments._Memejob__OclElement /* 1 */,
				Fragments._Memejob__MixinBase /* 2 */,
				Fragments._Memejob__Component /* 3 */,
				Fragments._Memejob__Sharkjob /* 4 */,
				Fragments._Memejob__Memejob /* 5 */
			};
		private static final int /*@NonNull*/ [] __Memejob = { 1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Memeshark =
			{
				Fragments._Memeshark__OclAny /* 0 */,
				Fragments._Memeshark__OclElement /* 1 */,
				Fragments._Memeshark__MixinBase /* 2 */,
				Fragments._Memeshark__Component /* 3 */,
				Fragments._Memeshark__Memeshark /* 4 */
			};
		private static final int /*@NonNull*/ [] __Memeshark = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Mongoquery =
			{
				Fragments._Mongoquery__OclAny /* 0 */,
				Fragments._Mongoquery__OclElement /* 1 */,
				Fragments._Mongoquery__MixinBase /* 2 */,
				Fragments._Mongoquery__Component /* 3 */,
				Fragments._Mongoquery__Databasequery /* 4 */,
				Fragments._Mongoquery__Mongoquery /* 5 */
			};
		private static final int /*@NonNull*/ [] __Mongoquery = { 1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Sharkjob =
			{
				Fragments._Sharkjob__OclAny /* 0 */,
				Fragments._Sharkjob__OclElement /* 1 */,
				Fragments._Sharkjob__MixinBase /* 2 */,
				Fragments._Sharkjob__Component /* 3 */,
				Fragments._Sharkjob__Sharkjob /* 4 */
			};
		private static final int /*@NonNull*/ [] __Sharkjob = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Sharkjobrevision =
			{
				Fragments._Sharkjobrevision__OclAny /* 0 */,
				Fragments._Sharkjobrevision__OclElement /* 1 */,
				Fragments._Sharkjobrevision__MixinBase /* 2 */,
				Fragments._Sharkjobrevision__Component /* 3 */,
				Fragments._Sharkjobrevision__Sharkjob /* 4 */,
				Fragments._Sharkjobrevision__Sharkjobrevision /* 5 */
			};
		private static final int /*@NonNull*/ [] __Sharkjobrevision = { 1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _String =
			{
				Fragments._String__OclAny /* 0 */,
				Fragments._String__OclComparable /* 1 */,
				Fragments._String__OclSummable /* 1 */,
				Fragments._String__String /* 2 */
			};
		private static final int /*@NonNull*/ [] __String = { 1,2,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Vcsjob =
			{
				Fragments._Vcsjob__OclAny /* 0 */,
				Fragments._Vcsjob__OclElement /* 1 */,
				Fragments._Vcsjob__MixinBase /* 2 */,
				Fragments._Vcsjob__Component /* 3 */,
				Fragments._Vcsjob__Sharkjob /* 4 */,
				Fragments._Vcsjob__Vcsjob /* 5 */
			};
		private static final int /*@NonNull*/ [] __Vcsjob = { 1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Vcsshark =
			{
				Fragments._Vcsshark__OclAny /* 0 */,
				Fragments._Vcsshark__OclElement /* 1 */,
				Fragments._Vcsshark__MixinBase /* 2 */,
				Fragments._Vcsshark__Component /* 3 */,
				Fragments._Vcsshark__Vcsshark /* 4 */
			};
		private static final int /*@NonNull*/ [] __Vcsshark = { 1,1,1,1,1 };

		/**
		 *	Install the fragment descriptors in the class descriptors.
		 */
		static {
			Types._Boolean.initFragments(_Boolean, __Boolean);
			Types._Coastjob.initFragments(_Coastjob, __Coastjob);
			Types._Coastshark.initFragments(_Coastshark, __Coastshark);
			Types._Databasedependency.initFragments(_Databasedependency, __Databasedependency);
			Types._Databasequery.initFragments(_Databasequery, __Databasequery);
			Types._Integer.initFragments(_Integer, __Integer);
			Types._Mecojob.initFragments(_Mecojob, __Mecojob);
			Types._Mecoshark.initFragments(_Mecoshark, __Mecoshark);
			Types._Memejob.initFragments(_Memejob, __Memejob);
			Types._Memeshark.initFragments(_Memeshark, __Memeshark);
			Types._Mongoquery.initFragments(_Mongoquery, __Mongoquery);
			Types._Sharkjob.initFragments(_Sharkjob, __Sharkjob);
			Types._Sharkjobrevision.initFragments(_Sharkjobrevision, __Sharkjobrevision);
			Types._String.initFragments(_String, __String);
			Types._Vcsjob.initFragments(_Vcsjob, __Vcsjob);
			Types._Vcsshark.initFragments(_Vcsshark, __Vcsshark);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::TypeFragments and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of local operations or local operation overrides for each fragment of each type.
	 */
	public static class FragmentOperations {
		static {
			Init.initStart();
			TypeFragments.init();
		}

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Boolean__Boolean = {
			OCLstdlibTables.Operations._Boolean___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Boolean___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._Boolean__and /* _'and'(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__implies /* _'implies'(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__not /* _'not'() */,
			OCLstdlibTables.Operations._Boolean__or /* _'or'(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__xor /* _'xor'(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._Boolean__and2 /* and2(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__implies2 /* implies2(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__not2 /* not2() */,
			OCLstdlibTables.Operations._Boolean__or2 /* or2(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__toString /* toString() */,
			OCLstdlibTables.Operations._Boolean__xor2 /* xor2(Boolean[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Boolean__OclAny = {
			OCLstdlibTables.Operations._Boolean___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Boolean___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._Boolean__toString /* toString() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastjob__Coastjob = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastjob__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastjob__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastjob__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastjob__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastjob__Sharkjob = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastjob__Sharkjobrevision = {};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastshark__Coastshark = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastshark__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastshark__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastshark__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Coastshark__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasedependency__Databasedependency = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasedependency__Dependency = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasedependency__Executiondependency = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasedependency__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasedependency__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasedependency__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasequery__Databasequery = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasequery__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasequery__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasequery__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Databasequery__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__Integer = {
			OCLstdlibTables.Operations._Integer___mul_ /* _'*'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___add_ /* _'+'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___neg_ /* _'-'() */,
			OCLstdlibTables.Operations._Integer___sub_ /* _'-'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___div_ /* _'/'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__abs /* abs() */,
			OCLstdlibTables.Operations._Integer__div /* div(Integer[?]) */,
			OCLstdlibTables.Operations._Integer__max /* max(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__min /* min(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__mod /* mod(Integer[?]) */,
			OCLstdlibTables.Operations._Integer__toString /* toString() */,
			OCLstdlibTables.Operations._Integer__toUnlimitedNatural /* toUnlimitedNatural() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__OclAny = {
			OCLstdlibTables.Operations._Real___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Real___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._Integer__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__OclComparable = {
			OCLstdlibTables.Operations._OclComparable___lt_ /* _'<'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclComparable___lt__eq_ /* _'<='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclComparable___gt_ /* _'>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclComparable___gt__eq_ /* _'>='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclComparable__compareTo /* compareTo(OclSelf[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__OclSummable = {
			OCLstdlibTables.Operations._OclSummable__sum /* sum(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclSummable__zero /* zero() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__Real = {
			OCLstdlibTables.Operations._Integer___mul_ /* _'*'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___add_ /* _'+'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___neg_ /* _'-'() */,
			OCLstdlibTables.Operations._Integer___sub_ /* _'-'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___div_ /* _'/'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Real___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Real___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__abs /* abs() */,
			OCLstdlibTables.Operations._Real__floor /* floor() */,
			OCLstdlibTables.Operations._Integer__max /* max(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__min /* min(OclSelf[?]) */,
			OCLstdlibTables.Operations._Real__round /* round() */,
			OCLstdlibTables.Operations._Integer__toString /* toString() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecojob__Mecojob = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecojob__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecojob__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecojob__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecojob__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecojob__Sharkjob = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecojob__Sharkjobrevision = {};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecoshark__Mecoshark = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecoshark__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecoshark__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecoshark__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mecoshark__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memejob__Memejob = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memejob__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memejob__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memejob__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memejob__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memejob__Sharkjob = {};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memeshark__Memeshark = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memeshark__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memeshark__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memeshark__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Memeshark__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mongoquery__Mongoquery = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mongoquery__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mongoquery__Databasequery = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mongoquery__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mongoquery__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Mongoquery__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjob__Sharkjob = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjob__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjob__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjob__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjob__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjobrevision__Sharkjobrevision = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjobrevision__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjobrevision__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjobrevision__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjobrevision__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Sharkjobrevision__Sharkjob = {};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _String__String = {
			OCLstdlibTables.Operations._String___add_ /* _'+'(String[?]) */,
			OCLstdlibTables.Operations._String___lt_ /* _'<'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___lt__eq_ /* _'<='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___gt_ /* _'>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___gt__eq_ /* _'>='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String__at /* at(Integer[?]) */,
			OCLstdlibTables.Operations._String__characters /* characters() */,
			OCLstdlibTables.Operations._String__compareTo /* compareTo(OclSelf[?]) */,
			OCLstdlibTables.Operations._String__concat /* concat(String[?]) */,
			OCLstdlibTables.Operations._String__endsWith /* endsWith(String[?]) */,
			OCLstdlibTables.Operations._String__equalsIgnoreCase /* equalsIgnoreCase(String[?]) */,
			//OCLstdlibTables.Operations._String__getSeverity /* getSeverity() */,
			OCLstdlibTables.Operations._String__indexOf /* indexOf(String[?]) */,
			OCLstdlibTables.Operations._String__lastIndexOf /* lastIndexOf(String[?]) */,
			//OCLstdlibTables.Operations._String__0_logDiagnostic /* logDiagnostic(OclAny[1],OclAny[?],OclAny[?],Integer[1],Boolean[?],Integer[1]) */,
			//OCLstdlibTables.Operations._String__1_logDiagnostic /* logDiagnostic(OclAny[1],OclAny[?],OclAny[?],OclAny[?],String[?],Integer[1],OclAny[?],Integer[1]) */,
			OCLstdlibTables.Operations._String__matches /* matches(String[?]) */,
			OCLstdlibTables.Operations._String__replaceAll /* replaceAll(String[?],String[?]) */,
			OCLstdlibTables.Operations._String__replaceFirst /* replaceFirst(String[?],String[?]) */,
			OCLstdlibTables.Operations._String__size /* size() */,
			OCLstdlibTables.Operations._String__startsWith /* startsWith(String[?]) */,
			OCLstdlibTables.Operations._String__substituteAll /* substituteAll(String[?],String[?]) */,
			OCLstdlibTables.Operations._String__substituteFirst /* substituteFirst(String[?],String[?]) */,
			OCLstdlibTables.Operations._String__substring /* substring(Integer[?],Integer[?]) */,
			OCLstdlibTables.Operations._String__toBoolean /* toBoolean() */,
			OCLstdlibTables.Operations._String__toInteger /* toInteger() */,
			OCLstdlibTables.Operations._String__toLower /* toLower() */,
			OCLstdlibTables.Operations._String__toLowerCase /* toLowerCase() */,
			OCLstdlibTables.Operations._String__toReal /* toReal() */,
			OCLstdlibTables.Operations._String__toString /* toString() */,
			OCLstdlibTables.Operations._String__toUpper /* toUpper() */,
			OCLstdlibTables.Operations._String__toUpperCase /* toUpperCase() */,
			OCLstdlibTables.Operations._String__0_tokenize /* tokenize() */,
			OCLstdlibTables.Operations._String__1_tokenize /* tokenize(String[?]) */,
			OCLstdlibTables.Operations._String__2_tokenize /* tokenize(String[?],Boolean[?]) */,
			OCLstdlibTables.Operations._String__trim /* trim() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _String__OclAny = {
			OCLstdlibTables.Operations._String___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._String__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _String__OclComparable = {
			OCLstdlibTables.Operations._String___lt_ /* _'<'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___lt__eq_ /* _'<='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___gt_ /* _'>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___gt__eq_ /* _'>='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String__compareTo /* compareTo(OclSelf[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _String__OclSummable = {
			OCLstdlibTables.Operations._OclSummable__sum /* sum(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclSummable__zero /* zero() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsjob__Vcsjob = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsjob__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsjob__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsjob__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsjob__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsjob__Sharkjob = {};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsshark__Vcsshark = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsshark__Component = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsshark__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsshark__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Vcsshark__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		/*
		 *	Install the operation descriptors in the fragment descriptors.
		 */
		static {
			Fragments._Boolean__Boolean.initOperations(_Boolean__Boolean);
			Fragments._Boolean__OclAny.initOperations(_Boolean__OclAny);

			Fragments._Coastjob__Coastjob.initOperations(_Coastjob__Coastjob);
			Fragments._Coastjob__Component.initOperations(_Coastjob__Component);
			Fragments._Coastjob__MixinBase.initOperations(_Coastjob__MixinBase);
			Fragments._Coastjob__OclAny.initOperations(_Coastjob__OclAny);
			Fragments._Coastjob__OclElement.initOperations(_Coastjob__OclElement);
			Fragments._Coastjob__Sharkjob.initOperations(_Coastjob__Sharkjob);
			Fragments._Coastjob__Sharkjobrevision.initOperations(_Coastjob__Sharkjobrevision);

			Fragments._Coastshark__Coastshark.initOperations(_Coastshark__Coastshark);
			Fragments._Coastshark__Component.initOperations(_Coastshark__Component);
			Fragments._Coastshark__MixinBase.initOperations(_Coastshark__MixinBase);
			Fragments._Coastshark__OclAny.initOperations(_Coastshark__OclAny);
			Fragments._Coastshark__OclElement.initOperations(_Coastshark__OclElement);

			Fragments._Databasedependency__Databasedependency.initOperations(_Databasedependency__Databasedependency);
			Fragments._Databasedependency__Dependency.initOperations(_Databasedependency__Dependency);
			Fragments._Databasedependency__Executiondependency.initOperations(_Databasedependency__Executiondependency);
			Fragments._Databasedependency__MixinBase.initOperations(_Databasedependency__MixinBase);
			Fragments._Databasedependency__OclAny.initOperations(_Databasedependency__OclAny);
			Fragments._Databasedependency__OclElement.initOperations(_Databasedependency__OclElement);

			Fragments._Databasequery__Component.initOperations(_Databasequery__Component);
			Fragments._Databasequery__Databasequery.initOperations(_Databasequery__Databasequery);
			Fragments._Databasequery__MixinBase.initOperations(_Databasequery__MixinBase);
			Fragments._Databasequery__OclAny.initOperations(_Databasequery__OclAny);
			Fragments._Databasequery__OclElement.initOperations(_Databasequery__OclElement);

			Fragments._Integer__Integer.initOperations(_Integer__Integer);
			Fragments._Integer__OclAny.initOperations(_Integer__OclAny);
			Fragments._Integer__OclComparable.initOperations(_Integer__OclComparable);
			Fragments._Integer__OclSummable.initOperations(_Integer__OclSummable);
			Fragments._Integer__Real.initOperations(_Integer__Real);

			Fragments._Mecojob__Component.initOperations(_Mecojob__Component);
			Fragments._Mecojob__Mecojob.initOperations(_Mecojob__Mecojob);
			Fragments._Mecojob__MixinBase.initOperations(_Mecojob__MixinBase);
			Fragments._Mecojob__OclAny.initOperations(_Mecojob__OclAny);
			Fragments._Mecojob__OclElement.initOperations(_Mecojob__OclElement);
			Fragments._Mecojob__Sharkjob.initOperations(_Mecojob__Sharkjob);
			Fragments._Mecojob__Sharkjobrevision.initOperations(_Mecojob__Sharkjobrevision);

			Fragments._Mecoshark__Component.initOperations(_Mecoshark__Component);
			Fragments._Mecoshark__Mecoshark.initOperations(_Mecoshark__Mecoshark);
			Fragments._Mecoshark__MixinBase.initOperations(_Mecoshark__MixinBase);
			Fragments._Mecoshark__OclAny.initOperations(_Mecoshark__OclAny);
			Fragments._Mecoshark__OclElement.initOperations(_Mecoshark__OclElement);

			Fragments._Memejob__Component.initOperations(_Memejob__Component);
			Fragments._Memejob__Memejob.initOperations(_Memejob__Memejob);
			Fragments._Memejob__MixinBase.initOperations(_Memejob__MixinBase);
			Fragments._Memejob__OclAny.initOperations(_Memejob__OclAny);
			Fragments._Memejob__OclElement.initOperations(_Memejob__OclElement);
			Fragments._Memejob__Sharkjob.initOperations(_Memejob__Sharkjob);

			Fragments._Memeshark__Component.initOperations(_Memeshark__Component);
			Fragments._Memeshark__Memeshark.initOperations(_Memeshark__Memeshark);
			Fragments._Memeshark__MixinBase.initOperations(_Memeshark__MixinBase);
			Fragments._Memeshark__OclAny.initOperations(_Memeshark__OclAny);
			Fragments._Memeshark__OclElement.initOperations(_Memeshark__OclElement);

			Fragments._Mongoquery__Component.initOperations(_Mongoquery__Component);
			Fragments._Mongoquery__Databasequery.initOperations(_Mongoquery__Databasequery);
			Fragments._Mongoquery__MixinBase.initOperations(_Mongoquery__MixinBase);
			Fragments._Mongoquery__Mongoquery.initOperations(_Mongoquery__Mongoquery);
			Fragments._Mongoquery__OclAny.initOperations(_Mongoquery__OclAny);
			Fragments._Mongoquery__OclElement.initOperations(_Mongoquery__OclElement);

			Fragments._Sharkjob__Component.initOperations(_Sharkjob__Component);
			Fragments._Sharkjob__MixinBase.initOperations(_Sharkjob__MixinBase);
			Fragments._Sharkjob__OclAny.initOperations(_Sharkjob__OclAny);
			Fragments._Sharkjob__OclElement.initOperations(_Sharkjob__OclElement);
			Fragments._Sharkjob__Sharkjob.initOperations(_Sharkjob__Sharkjob);

			Fragments._Sharkjobrevision__Component.initOperations(_Sharkjobrevision__Component);
			Fragments._Sharkjobrevision__MixinBase.initOperations(_Sharkjobrevision__MixinBase);
			Fragments._Sharkjobrevision__OclAny.initOperations(_Sharkjobrevision__OclAny);
			Fragments._Sharkjobrevision__OclElement.initOperations(_Sharkjobrevision__OclElement);
			Fragments._Sharkjobrevision__Sharkjob.initOperations(_Sharkjobrevision__Sharkjob);
			Fragments._Sharkjobrevision__Sharkjobrevision.initOperations(_Sharkjobrevision__Sharkjobrevision);

			Fragments._String__OclAny.initOperations(_String__OclAny);
			Fragments._String__OclComparable.initOperations(_String__OclComparable);
			Fragments._String__OclSummable.initOperations(_String__OclSummable);
			Fragments._String__String.initOperations(_String__String);

			Fragments._Vcsjob__Component.initOperations(_Vcsjob__Component);
			Fragments._Vcsjob__MixinBase.initOperations(_Vcsjob__MixinBase);
			Fragments._Vcsjob__OclAny.initOperations(_Vcsjob__OclAny);
			Fragments._Vcsjob__OclElement.initOperations(_Vcsjob__OclElement);
			Fragments._Vcsjob__Sharkjob.initOperations(_Vcsjob__Sharkjob);
			Fragments._Vcsjob__Vcsjob.initOperations(_Vcsjob__Vcsjob);

			Fragments._Vcsshark__Component.initOperations(_Vcsshark__Component);
			Fragments._Vcsshark__MixinBase.initOperations(_Vcsshark__MixinBase);
			Fragments._Vcsshark__OclAny.initOperations(_Vcsshark__OclAny);
			Fragments._Vcsshark__OclElement.initOperations(_Vcsshark__OclElement);
			Fragments._Vcsshark__Vcsshark.initOperations(_Vcsshark__Vcsshark);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::FragmentOperations and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of local properties for the local fragment of each type.
	 */
	public static class FragmentProperties {
		static {
			Init.initStart();
			FragmentOperations.init();
		}

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Boolean = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Coastjob = {
			SharkTables.Properties._Coastjob__coastMethodMetrics,
			SharkTables.Properties._Sharkjob__sharkLogLevel,
			SharkTables.Properties._Sharkjob__sharkProjectDirectory,
			SharkTables.Properties._Sharkjob__sharkProjectName,
			SharkTables.Properties._Sharkjob__sharkProjectRepository,
			SharkTables.Properties._Sharkjobrevision__sharkProjectRevision
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Coastshark = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Databasedependency = {
			SharkTables.Properties._Databasedependency__databaseAuthentication,
			SharkTables.Properties._Databasedependency__databaseHostname,
			SharkTables.Properties._Databasedependency__databaseName,
			SharkTables.Properties._Databasedependency__databasePassword,
			SharkTables.Properties._Databasedependency__databasePort,
			SharkTables.Properties._Databasedependency__databaseSslConnection,
			SharkTables.Properties._Databasedependency__databaseUser,
			SharkTables.Properties._Databasedependency__dbDriver
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Databasequery = {
			SharkTables.Properties._Databasequery__databaseName
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Integer = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Mecojob = {
			SharkTables.Properties._Mecojob__mecoOutputDirectory,
			SharkTables.Properties._Sharkjob__sharkLogLevel,
			SharkTables.Properties._Sharkjob__sharkProjectDirectory,
			SharkTables.Properties._Sharkjob__sharkProjectName,
			SharkTables.Properties._Sharkjob__sharkProjectRepository,
			SharkTables.Properties._Sharkjobrevision__sharkProjectRevision
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Mecoshark = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Memejob = {
			SharkTables.Properties._Memejob__memeParallelProcesses,
			SharkTables.Properties._Sharkjob__sharkLogLevel,
			SharkTables.Properties._Sharkjob__sharkProjectDirectory,
			SharkTables.Properties._Sharkjob__sharkProjectName,
			SharkTables.Properties._Sharkjob__sharkProjectRepository
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Memeshark = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Mongoquery = {
			SharkTables.Properties._Databasequery__databaseName,
			SharkTables.Properties._Mongoquery__mongoEvalJs
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Sharkjob = {
			SharkTables.Properties._Sharkjob__sharkLogLevel,
			SharkTables.Properties._Sharkjob__sharkProjectDirectory,
			SharkTables.Properties._Sharkjob__sharkProjectName,
			SharkTables.Properties._Sharkjob__sharkProjectRepository
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Sharkjobrevision = {
			SharkTables.Properties._Sharkjob__sharkLogLevel,
			SharkTables.Properties._Sharkjob__sharkProjectDirectory,
			SharkTables.Properties._Sharkjob__sharkProjectName,
			SharkTables.Properties._Sharkjob__sharkProjectRepository,
			SharkTables.Properties._Sharkjobrevision__sharkProjectRevision
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _String = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Vcsjob = {
			SharkTables.Properties._Sharkjob__sharkLogLevel,
			SharkTables.Properties._Sharkjob__sharkProjectDirectory,
			SharkTables.Properties._Sharkjob__sharkProjectName,
			SharkTables.Properties._Sharkjob__sharkProjectRepository,
			SharkTables.Properties._Vcsjob__vcsUsedcores
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Vcsshark = {};

		/**
		 *	Install the property descriptors in the fragment descriptors.
		 */
		static {
			Fragments._Boolean__Boolean.initProperties(_Boolean);
			Fragments._Coastjob__Coastjob.initProperties(_Coastjob);
			Fragments._Coastshark__Coastshark.initProperties(_Coastshark);
			Fragments._Databasedependency__Databasedependency.initProperties(_Databasedependency);
			Fragments._Databasequery__Databasequery.initProperties(_Databasequery);
			Fragments._Integer__Integer.initProperties(_Integer);
			Fragments._Mecojob__Mecojob.initProperties(_Mecojob);
			Fragments._Mecoshark__Mecoshark.initProperties(_Mecoshark);
			Fragments._Memejob__Memejob.initProperties(_Memejob);
			Fragments._Memeshark__Memeshark.initProperties(_Memeshark);
			Fragments._Mongoquery__Mongoquery.initProperties(_Mongoquery);
			Fragments._Sharkjob__Sharkjob.initProperties(_Sharkjob);
			Fragments._Sharkjobrevision__Sharkjobrevision.initProperties(_Sharkjobrevision);
			Fragments._String__String.initProperties(_String);
			Fragments._Vcsjob__Vcsjob.initProperties(_Vcsjob);
			Fragments._Vcsshark__Vcsshark.initProperties(_Vcsshark);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::FragmentProperties and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of enumeration literals for each enumeration.
	 */
	public static class EnumerationLiterals {
		static {
			Init.initStart();
			FragmentProperties.init();
		}

		/**
		 *	Install the enumeration literals in the enumerations.
		 */
		static {

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of SharkTables::EnumerationLiterals and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 * The multiple packages above avoid problems with the Java 65536 byte limit but introduce a difficulty in ensuring that
	 * static construction occurs in the disciplined order of the packages when construction may start in any of the packages.
	 * The problem is resolved by ensuring that the static construction of each package first initializes its immediate predecessor.
	 * On completion of predecessor initialization, the residual packages are initialized by starting an initialization in the last package.
	 * This class maintains a count so that the various predecessors can distinguish whether they are the starting point and so
	 * ensure that residual construction occurs just once after all predecessors.
	 */
	private static class Init {
		/**
		 * Counter of nested static constructions. On return to zero residual construction starts. -ve once residual construction started.
		 */
		private static int initCount = 0;

		/**
		 * Invoked at the start of a static construction to defer residual cobstruction until primary constructions complete.
		 */
		private static void initStart() {
			if (initCount >= 0) {
				initCount++;
			}
		}

		/**
		 * Invoked at the end of a static construction to activate residual cobstruction once primary constructions complete.
		 */
		private static void initEnd() {
			if (initCount > 0) {
				if (--initCount == 0) {
					initCount = -1;
					EnumerationLiterals.init();
				}
			}
		}
	}

	static {
		Init.initEnd();
	}

	/*
	 * Force initialization of outer fields. Inner fields are lazily initialized.
	 */
	public static void init() {}
}
