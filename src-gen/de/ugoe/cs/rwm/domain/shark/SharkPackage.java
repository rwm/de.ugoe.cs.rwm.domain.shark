/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark;

import modmacao.ModmacaoPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.rwm.domain.shark.SharkFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore"
 * @generated
 */
public interface SharkPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "shark";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://schemas.ugoe.cs.rwm/domain/shark/ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "shark";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SharkPackage eINSTANCE = de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobImpl <em>Sharkjob</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkjobImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getSharkjob()
	 * @generated
	 */
	int SHARKJOB = 0;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB__MIXIN = ModmacaoPackage.COMPONENT__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB__ENTITY = ModmacaoPackage.COMPONENT__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB__ATTRIBUTES = ModmacaoPackage.COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB__MODMACAO_COMPONENT_VERSION = ModmacaoPackage.COMPONENT__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Shark Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB__SHARK_LOG_LEVEL = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Shark Project Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB__SHARK_PROJECT_REPOSITORY = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Shark Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB__SHARK_PROJECT_NAME = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Shark Project Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB__SHARK_PROJECT_DIRECTORY = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Sharkjob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB_FEATURE_COUNT = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Sharkjob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOB_OPERATION_COUNT = ModmacaoPackage.COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.VcsjobImpl <em>Vcsjob</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.VcsjobImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getVcsjob()
	 * @generated
	 */
	int VCSJOB = 1;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__MIXIN = SHARKJOB__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__ENTITY = SHARKJOB__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__ATTRIBUTES = SHARKJOB__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__MODMACAO_COMPONENT_VERSION = SHARKJOB__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Shark Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__SHARK_LOG_LEVEL = SHARKJOB__SHARK_LOG_LEVEL;

	/**
	 * The feature id for the '<em><b>Shark Project Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__SHARK_PROJECT_REPOSITORY = SHARKJOB__SHARK_PROJECT_REPOSITORY;

	/**
	 * The feature id for the '<em><b>Shark Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__SHARK_PROJECT_NAME = SHARKJOB__SHARK_PROJECT_NAME;

	/**
	 * The feature id for the '<em><b>Shark Project Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__SHARK_PROJECT_DIRECTORY = SHARKJOB__SHARK_PROJECT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Vcs Usedcores</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB__VCS_USEDCORES = SHARKJOB_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Vcsjob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB_FEATURE_COUNT = SHARKJOB_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Vcsjob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSJOB_OPERATION_COUNT = SHARKJOB_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobrevisionImpl <em>Sharkjobrevision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkjobrevisionImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getSharkjobrevision()
	 * @generated
	 */
	int SHARKJOBREVISION = 6;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__MIXIN = SHARKJOB__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__ENTITY = SHARKJOB__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__ATTRIBUTES = SHARKJOB__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__MODMACAO_COMPONENT_VERSION = SHARKJOB__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Shark Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__SHARK_LOG_LEVEL = SHARKJOB__SHARK_LOG_LEVEL;

	/**
	 * The feature id for the '<em><b>Shark Project Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__SHARK_PROJECT_REPOSITORY = SHARKJOB__SHARK_PROJECT_REPOSITORY;

	/**
	 * The feature id for the '<em><b>Shark Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__SHARK_PROJECT_NAME = SHARKJOB__SHARK_PROJECT_NAME;

	/**
	 * The feature id for the '<em><b>Shark Project Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__SHARK_PROJECT_DIRECTORY = SHARKJOB__SHARK_PROJECT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Shark Project Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION__SHARK_PROJECT_REVISION = SHARKJOB_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sharkjobrevision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION_FEATURE_COUNT = SHARKJOB_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Sharkjobrevision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARKJOBREVISION_OPERATION_COUNT = SHARKJOB_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MecojobImpl <em>Mecojob</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.MecojobImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMecojob()
	 * @generated
	 */
	int MECOJOB = 2;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__MIXIN = SHARKJOBREVISION__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__ENTITY = SHARKJOBREVISION__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__ATTRIBUTES = SHARKJOBREVISION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__MODMACAO_COMPONENT_VERSION = SHARKJOBREVISION__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Shark Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__SHARK_LOG_LEVEL = SHARKJOBREVISION__SHARK_LOG_LEVEL;

	/**
	 * The feature id for the '<em><b>Shark Project Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__SHARK_PROJECT_REPOSITORY = SHARKJOBREVISION__SHARK_PROJECT_REPOSITORY;

	/**
	 * The feature id for the '<em><b>Shark Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__SHARK_PROJECT_NAME = SHARKJOBREVISION__SHARK_PROJECT_NAME;

	/**
	 * The feature id for the '<em><b>Shark Project Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__SHARK_PROJECT_DIRECTORY = SHARKJOBREVISION__SHARK_PROJECT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Shark Project Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__SHARK_PROJECT_REVISION = SHARKJOBREVISION__SHARK_PROJECT_REVISION;

	/**
	 * The feature id for the '<em><b>Meco Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB__MECO_OUTPUT_DIRECTORY = SHARKJOBREVISION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Mecojob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB_FEATURE_COUNT = SHARKJOBREVISION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Mecojob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOJOB_OPERATION_COUNT = SHARKJOBREVISION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.CoastjobImpl <em>Coastjob</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.CoastjobImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getCoastjob()
	 * @generated
	 */
	int COASTJOB = 3;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__MIXIN = SHARKJOBREVISION__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__ENTITY = SHARKJOBREVISION__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__ATTRIBUTES = SHARKJOBREVISION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__MODMACAO_COMPONENT_VERSION = SHARKJOBREVISION__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Shark Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__SHARK_LOG_LEVEL = SHARKJOBREVISION__SHARK_LOG_LEVEL;

	/**
	 * The feature id for the '<em><b>Shark Project Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__SHARK_PROJECT_REPOSITORY = SHARKJOBREVISION__SHARK_PROJECT_REPOSITORY;

	/**
	 * The feature id for the '<em><b>Shark Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__SHARK_PROJECT_NAME = SHARKJOBREVISION__SHARK_PROJECT_NAME;

	/**
	 * The feature id for the '<em><b>Shark Project Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__SHARK_PROJECT_DIRECTORY = SHARKJOBREVISION__SHARK_PROJECT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Shark Project Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__SHARK_PROJECT_REVISION = SHARKJOBREVISION__SHARK_PROJECT_REVISION;

	/**
	 * The feature id for the '<em><b>Coast Method Metrics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB__COAST_METHOD_METRICS = SHARKJOBREVISION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Coastjob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB_FEATURE_COUNT = SHARKJOBREVISION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Coastjob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTJOB_OPERATION_COUNT = SHARKJOBREVISION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MemejobImpl <em>Memejob</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.MemejobImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMemejob()
	 * @generated
	 */
	int MEMEJOB = 4;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__MIXIN = SHARKJOB__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__ENTITY = SHARKJOB__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__ATTRIBUTES = SHARKJOB__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__MODMACAO_COMPONENT_VERSION = SHARKJOB__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Shark Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__SHARK_LOG_LEVEL = SHARKJOB__SHARK_LOG_LEVEL;

	/**
	 * The feature id for the '<em><b>Shark Project Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__SHARK_PROJECT_REPOSITORY = SHARKJOB__SHARK_PROJECT_REPOSITORY;

	/**
	 * The feature id for the '<em><b>Shark Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__SHARK_PROJECT_NAME = SHARKJOB__SHARK_PROJECT_NAME;

	/**
	 * The feature id for the '<em><b>Shark Project Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__SHARK_PROJECT_DIRECTORY = SHARKJOB__SHARK_PROJECT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Meme Parallel Processes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB__MEME_PARALLEL_PROCESSES = SHARKJOB_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Memejob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB_FEATURE_COUNT = SHARKJOB_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Memejob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMEJOB_OPERATION_COUNT = SHARKJOB_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl <em>Databasedependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getDatabasedependency()
	 * @generated
	 */
	int DATABASEDEPENDENCY = 5;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__MIXIN = ModmacaoPackage.EXECUTIONDEPENDENCY__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__ENTITY = ModmacaoPackage.EXECUTIONDEPENDENCY__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__ATTRIBUTES = ModmacaoPackage.EXECUTIONDEPENDENCY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Database User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__DATABASE_USER = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Database Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__DATABASE_PASSWORD = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Database Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__DATABASE_NAME = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Database Hostname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__DATABASE_HOSTNAME = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Database Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__DATABASE_PORT = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Database Authentication</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__DATABASE_AUTHENTICATION = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Database Ssl Connection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Db Driver</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY__DB_DRIVER = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Databasedependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY_FEATURE_COUNT = ModmacaoPackage.EXECUTIONDEPENDENCY_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Databasedependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEDEPENDENCY_OPERATION_COUNT = ModmacaoPackage.EXECUTIONDEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.VcssharkImpl <em>Vcsshark</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.VcssharkImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getVcsshark()
	 * @generated
	 */
	int VCSSHARK = 7;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSSHARK__MIXIN = ModmacaoPackage.COMPONENT__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSSHARK__ENTITY = ModmacaoPackage.COMPONENT__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSSHARK__ATTRIBUTES = ModmacaoPackage.COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSSHARK__MODMACAO_COMPONENT_VERSION = ModmacaoPackage.COMPONENT__MODMACAO_COMPONENT_VERSION;

	/**
	 * The number of structural features of the '<em>Vcsshark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSSHARK_FEATURE_COUNT = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vcsshark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VCSSHARK_OPERATION_COUNT = ModmacaoPackage.COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MemesharkImpl <em>Memeshark</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.MemesharkImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMemeshark()
	 * @generated
	 */
	int MEMESHARK = 8;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMESHARK__MIXIN = ModmacaoPackage.COMPONENT__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMESHARK__ENTITY = ModmacaoPackage.COMPONENT__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMESHARK__ATTRIBUTES = ModmacaoPackage.COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMESHARK__MODMACAO_COMPONENT_VERSION = ModmacaoPackage.COMPONENT__MODMACAO_COMPONENT_VERSION;

	/**
	 * The number of structural features of the '<em>Memeshark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMESHARK_FEATURE_COUNT = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Memeshark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMESHARK_OPERATION_COUNT = ModmacaoPackage.COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MecosharkImpl <em>Mecoshark</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.MecosharkImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMecoshark()
	 * @generated
	 */
	int MECOSHARK = 9;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOSHARK__MIXIN = ModmacaoPackage.COMPONENT__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOSHARK__ENTITY = ModmacaoPackage.COMPONENT__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOSHARK__ATTRIBUTES = ModmacaoPackage.COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOSHARK__MODMACAO_COMPONENT_VERSION = ModmacaoPackage.COMPONENT__MODMACAO_COMPONENT_VERSION;

	/**
	 * The number of structural features of the '<em>Mecoshark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOSHARK_FEATURE_COUNT = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Mecoshark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MECOSHARK_OPERATION_COUNT = ModmacaoPackage.COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.CoastsharkImpl <em>Coastshark</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.CoastsharkImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getCoastshark()
	 * @generated
	 */
	int COASTSHARK = 10;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTSHARK__MIXIN = ModmacaoPackage.COMPONENT__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTSHARK__ENTITY = ModmacaoPackage.COMPONENT__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTSHARK__ATTRIBUTES = ModmacaoPackage.COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTSHARK__MODMACAO_COMPONENT_VERSION = ModmacaoPackage.COMPONENT__MODMACAO_COMPONENT_VERSION;

	/**
	 * The number of structural features of the '<em>Coastshark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTSHARK_FEATURE_COUNT = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Coastshark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COASTSHARK_OPERATION_COUNT = ModmacaoPackage.COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasequeryImpl <em>Databasequery</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.DatabasequeryImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getDatabasequery()
	 * @generated
	 */
	int DATABASEQUERY = 11;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEQUERY__MIXIN = ModmacaoPackage.COMPONENT__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEQUERY__ENTITY = ModmacaoPackage.COMPONENT__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEQUERY__ATTRIBUTES = ModmacaoPackage.COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEQUERY__MODMACAO_COMPONENT_VERSION = ModmacaoPackage.COMPONENT__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Database Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEQUERY__DATABASE_NAME = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Databasequery</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEQUERY_FEATURE_COUNT = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Databasequery</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASEQUERY_OPERATION_COUNT = ModmacaoPackage.COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MongoqueryImpl <em>Mongoquery</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.shark.impl.MongoqueryImpl
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMongoquery()
	 * @generated
	 */
	int MONGOQUERY = 12;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONGOQUERY__MIXIN = DATABASEQUERY__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONGOQUERY__ENTITY = DATABASEQUERY__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONGOQUERY__ATTRIBUTES = DATABASEQUERY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONGOQUERY__MODMACAO_COMPONENT_VERSION = DATABASEQUERY__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Database Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONGOQUERY__DATABASE_NAME = DATABASEQUERY__DATABASE_NAME;

	/**
	 * The feature id for the '<em><b>Mongo Eval Js</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONGOQUERY__MONGO_EVAL_JS = DATABASEQUERY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Mongoquery</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONGOQUERY_FEATURE_COUNT = DATABASEQUERY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Mongoquery</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONGOQUERY_OPERATION_COUNT = DATABASEQUERY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getString()
	 * @generated
	 */
	int STRING = 13;

	/**
	 * The meta object id for the '<em>Boolean</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Boolean
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getBoolean()
	 * @generated
	 */
	int BOOLEAN = 14;

	/**
	 * The meta object id for the '<em>Integer</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Integer
	 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getInteger()
	 * @generated
	 */
	int INTEGER = 15;


	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob <em>Sharkjob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sharkjob</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Sharkjob
	 * @generated
	 */
	EClass getSharkjob();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkLogLevel <em>Shark Log Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shark Log Level</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkLogLevel()
	 * @see #getSharkjob()
	 * @generated
	 */
	EAttribute getSharkjob_SharkLogLevel();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectRepository <em>Shark Project Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shark Project Repository</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectRepository()
	 * @see #getSharkjob()
	 * @generated
	 */
	EAttribute getSharkjob_SharkProjectRepository();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectName <em>Shark Project Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shark Project Name</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectName()
	 * @see #getSharkjob()
	 * @generated
	 */
	EAttribute getSharkjob_SharkProjectName();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectDirectory <em>Shark Project Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shark Project Directory</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectDirectory()
	 * @see #getSharkjob()
	 * @generated
	 */
	EAttribute getSharkjob_SharkProjectDirectory();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Vcsjob <em>Vcsjob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vcsjob</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Vcsjob
	 * @generated
	 */
	EClass getVcsjob();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Vcsjob#getVcsUsedcores <em>Vcs Usedcores</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vcs Usedcores</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Vcsjob#getVcsUsedcores()
	 * @see #getVcsjob()
	 * @generated
	 */
	EAttribute getVcsjob_VcsUsedcores();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Mecojob <em>Mecojob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mecojob</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Mecojob
	 * @generated
	 */
	EClass getMecojob();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Mecojob#getMecoOutputDirectory <em>Meco Output Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Meco Output Directory</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Mecojob#getMecoOutputDirectory()
	 * @see #getMecojob()
	 * @generated
	 */
	EAttribute getMecojob_MecoOutputDirectory();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Coastjob <em>Coastjob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coastjob</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Coastjob
	 * @generated
	 */
	EClass getCoastjob();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Coastjob#getCoastMethodMetrics <em>Coast Method Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Coast Method Metrics</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Coastjob#getCoastMethodMetrics()
	 * @see #getCoastjob()
	 * @generated
	 */
	EAttribute getCoastjob_CoastMethodMetrics();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Memejob <em>Memejob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Memejob</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Memejob
	 * @generated
	 */
	EClass getMemejob();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Memejob#getMemeParallelProcesses <em>Meme Parallel Processes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Meme Parallel Processes</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Memejob#getMemeParallelProcesses()
	 * @see #getMemejob()
	 * @generated
	 */
	EAttribute getMemejob_MemeParallelProcesses();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency <em>Databasedependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Databasedependency</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency
	 * @generated
	 */
	EClass getDatabasedependency();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseUser <em>Database User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database User</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseUser()
	 * @see #getDatabasedependency()
	 * @generated
	 */
	EAttribute getDatabasedependency_DatabaseUser();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabasePassword <em>Database Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database Password</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabasePassword()
	 * @see #getDatabasedependency()
	 * @generated
	 */
	EAttribute getDatabasedependency_DatabasePassword();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseName <em>Database Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database Name</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseName()
	 * @see #getDatabasedependency()
	 * @generated
	 */
	EAttribute getDatabasedependency_DatabaseName();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseHostname <em>Database Hostname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database Hostname</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseHostname()
	 * @see #getDatabasedependency()
	 * @generated
	 */
	EAttribute getDatabasedependency_DatabaseHostname();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabasePort <em>Database Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database Port</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabasePort()
	 * @see #getDatabasedependency()
	 * @generated
	 */
	EAttribute getDatabasedependency_DatabasePort();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseAuthentication <em>Database Authentication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database Authentication</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseAuthentication()
	 * @see #getDatabasedependency()
	 * @generated
	 */
	EAttribute getDatabasedependency_DatabaseAuthentication();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseSslConnection <em>Database Ssl Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database Ssl Connection</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseSslConnection()
	 * @see #getDatabasedependency()
	 * @generated
	 */
	EAttribute getDatabasedependency_DatabaseSslConnection();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDbDriver <em>Db Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Db Driver</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasedependency#getDbDriver()
	 * @see #getDatabasedependency()
	 * @generated
	 */
	EAttribute getDatabasedependency_DbDriver();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Sharkjobrevision <em>Sharkjobrevision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sharkjobrevision</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Sharkjobrevision
	 * @generated
	 */
	EClass getSharkjobrevision();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Sharkjobrevision#getSharkProjectRevision <em>Shark Project Revision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shark Project Revision</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Sharkjobrevision#getSharkProjectRevision()
	 * @see #getSharkjobrevision()
	 * @generated
	 */
	EAttribute getSharkjobrevision_SharkProjectRevision();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Vcsshark <em>Vcsshark</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vcsshark</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Vcsshark
	 * @generated
	 */
	EClass getVcsshark();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Memeshark <em>Memeshark</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Memeshark</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Memeshark
	 * @generated
	 */
	EClass getMemeshark();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Mecoshark <em>Mecoshark</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mecoshark</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Mecoshark
	 * @generated
	 */
	EClass getMecoshark();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Coastshark <em>Coastshark</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coastshark</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Coastshark
	 * @generated
	 */
	EClass getCoastshark();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Databasequery <em>Databasequery</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Databasequery</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasequery
	 * @generated
	 */
	EClass getDatabasequery();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Databasequery#getDatabaseName <em>Database Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database Name</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Databasequery#getDatabaseName()
	 * @see #getDatabasequery()
	 * @generated
	 */
	EAttribute getDatabasequery_DatabaseName();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.shark.Mongoquery <em>Mongoquery</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mongoquery</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Mongoquery
	 * @generated
	 */
	EClass getMongoquery();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.shark.Mongoquery#getMongoEvalJs <em>Mongo Eval Js</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mongo Eval Js</em>'.
	 * @see de.ugoe.cs.rwm.domain.shark.Mongoquery#getMongoEvalJs()
	 * @see #getMongoquery()
	 * @generated
	 */
	EAttribute getMongoquery_MongoEvalJs();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '{@link java.lang.Boolean <em>Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Boolean</em>'.
	 * @see java.lang.Boolean
	 * @model instanceClass="java.lang.Boolean"
	 * @generated
	 */
	EDataType getBoolean();

	/**
	 * Returns the meta object for data type '{@link java.lang.Integer <em>Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Integer</em>'.
	 * @see java.lang.Integer
	 * @model instanceClass="java.lang.Integer"
	 * @generated
	 */
	EDataType getInteger();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SharkFactory getSharkFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobImpl <em>Sharkjob</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkjobImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getSharkjob()
		 * @generated
		 */
		EClass SHARKJOB = eINSTANCE.getSharkjob();

		/**
		 * The meta object literal for the '<em><b>Shark Log Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHARKJOB__SHARK_LOG_LEVEL = eINSTANCE.getSharkjob_SharkLogLevel();

		/**
		 * The meta object literal for the '<em><b>Shark Project Repository</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHARKJOB__SHARK_PROJECT_REPOSITORY = eINSTANCE.getSharkjob_SharkProjectRepository();

		/**
		 * The meta object literal for the '<em><b>Shark Project Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHARKJOB__SHARK_PROJECT_NAME = eINSTANCE.getSharkjob_SharkProjectName();

		/**
		 * The meta object literal for the '<em><b>Shark Project Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHARKJOB__SHARK_PROJECT_DIRECTORY = eINSTANCE.getSharkjob_SharkProjectDirectory();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.VcsjobImpl <em>Vcsjob</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.VcsjobImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getVcsjob()
		 * @generated
		 */
		EClass VCSJOB = eINSTANCE.getVcsjob();

		/**
		 * The meta object literal for the '<em><b>Vcs Usedcores</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VCSJOB__VCS_USEDCORES = eINSTANCE.getVcsjob_VcsUsedcores();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MecojobImpl <em>Mecojob</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.MecojobImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMecojob()
		 * @generated
		 */
		EClass MECOJOB = eINSTANCE.getMecojob();

		/**
		 * The meta object literal for the '<em><b>Meco Output Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MECOJOB__MECO_OUTPUT_DIRECTORY = eINSTANCE.getMecojob_MecoOutputDirectory();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.CoastjobImpl <em>Coastjob</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.CoastjobImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getCoastjob()
		 * @generated
		 */
		EClass COASTJOB = eINSTANCE.getCoastjob();

		/**
		 * The meta object literal for the '<em><b>Coast Method Metrics</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COASTJOB__COAST_METHOD_METRICS = eINSTANCE.getCoastjob_CoastMethodMetrics();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MemejobImpl <em>Memejob</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.MemejobImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMemejob()
		 * @generated
		 */
		EClass MEMEJOB = eINSTANCE.getMemejob();

		/**
		 * The meta object literal for the '<em><b>Meme Parallel Processes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEMEJOB__MEME_PARALLEL_PROCESSES = eINSTANCE.getMemejob_MemeParallelProcesses();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl <em>Databasedependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getDatabasedependency()
		 * @generated
		 */
		EClass DATABASEDEPENDENCY = eINSTANCE.getDatabasedependency();

		/**
		 * The meta object literal for the '<em><b>Database User</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEDEPENDENCY__DATABASE_USER = eINSTANCE.getDatabasedependency_DatabaseUser();

		/**
		 * The meta object literal for the '<em><b>Database Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEDEPENDENCY__DATABASE_PASSWORD = eINSTANCE.getDatabasedependency_DatabasePassword();

		/**
		 * The meta object literal for the '<em><b>Database Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEDEPENDENCY__DATABASE_NAME = eINSTANCE.getDatabasedependency_DatabaseName();

		/**
		 * The meta object literal for the '<em><b>Database Hostname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEDEPENDENCY__DATABASE_HOSTNAME = eINSTANCE.getDatabasedependency_DatabaseHostname();

		/**
		 * The meta object literal for the '<em><b>Database Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEDEPENDENCY__DATABASE_PORT = eINSTANCE.getDatabasedependency_DatabasePort();

		/**
		 * The meta object literal for the '<em><b>Database Authentication</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEDEPENDENCY__DATABASE_AUTHENTICATION = eINSTANCE.getDatabasedependency_DatabaseAuthentication();

		/**
		 * The meta object literal for the '<em><b>Database Ssl Connection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION = eINSTANCE.getDatabasedependency_DatabaseSslConnection();

		/**
		 * The meta object literal for the '<em><b>Db Driver</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEDEPENDENCY__DB_DRIVER = eINSTANCE.getDatabasedependency_DbDriver();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobrevisionImpl <em>Sharkjobrevision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkjobrevisionImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getSharkjobrevision()
		 * @generated
		 */
		EClass SHARKJOBREVISION = eINSTANCE.getSharkjobrevision();

		/**
		 * The meta object literal for the '<em><b>Shark Project Revision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHARKJOBREVISION__SHARK_PROJECT_REVISION = eINSTANCE.getSharkjobrevision_SharkProjectRevision();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.VcssharkImpl <em>Vcsshark</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.VcssharkImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getVcsshark()
		 * @generated
		 */
		EClass VCSSHARK = eINSTANCE.getVcsshark();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MemesharkImpl <em>Memeshark</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.MemesharkImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMemeshark()
		 * @generated
		 */
		EClass MEMESHARK = eINSTANCE.getMemeshark();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MecosharkImpl <em>Mecoshark</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.MecosharkImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMecoshark()
		 * @generated
		 */
		EClass MECOSHARK = eINSTANCE.getMecoshark();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.CoastsharkImpl <em>Coastshark</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.CoastsharkImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getCoastshark()
		 * @generated
		 */
		EClass COASTSHARK = eINSTANCE.getCoastshark();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasequeryImpl <em>Databasequery</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.DatabasequeryImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getDatabasequery()
		 * @generated
		 */
		EClass DATABASEQUERY = eINSTANCE.getDatabasequery();

		/**
		 * The meta object literal for the '<em><b>Database Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASEQUERY__DATABASE_NAME = eINSTANCE.getDatabasequery_DatabaseName();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.shark.impl.MongoqueryImpl <em>Mongoquery</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.shark.impl.MongoqueryImpl
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getMongoquery()
		 * @generated
		 */
		EClass MONGOQUERY = eINSTANCE.getMongoquery();

		/**
		 * The meta object literal for the '<em><b>Mongo Eval Js</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONGOQUERY__MONGO_EVAL_JS = eINSTANCE.getMongoquery_MongoEvalJs();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>Boolean</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Boolean
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getBoolean()
		 * @generated
		 */
		EDataType BOOLEAN = eINSTANCE.getBoolean();

		/**
		 * The meta object literal for the '<em>Integer</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Integer
		 * @see de.ugoe.cs.rwm.domain.shark.impl.SharkPackageImpl#getInteger()
		 * @generated
		 */
		EDataType INTEGER = eINSTANCE.getInteger();

	}

} //SharkPackage
