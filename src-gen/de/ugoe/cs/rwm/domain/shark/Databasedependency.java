/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark;

import modmacao.Executiondependency;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Databasedependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseUser <em>Database User</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabasePassword <em>Database Password</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseName <em>Database Name</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseHostname <em>Database Hostname</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabasePort <em>Database Port</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseAuthentication <em>Database Authentication</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseSslConnection <em>Database Ssl Connection</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDbDriver <em>Db Driver</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency()
 * @model
 * @generated
 */
public interface Databasedependency extends Executiondependency, MixinBase {
	/**
	 * Returns the value of the '<em><b>Database User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Database User</em>' attribute.
	 * @see #setDatabaseUser(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency_DatabaseUser()
	 * @model dataType="de.ugoe.cs.rwm.domain.shark.String" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Databasedependency!databaseUser'"
	 * @generated
	 */
	String getDatabaseUser();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseUser <em>Database User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database User</em>' attribute.
	 * @see #getDatabaseUser()
	 * @generated
	 */
	void setDatabaseUser(String value);

	/**
	 * Returns the value of the '<em><b>Database Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Database Password</em>' attribute.
	 * @see #setDatabasePassword(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency_DatabasePassword()
	 * @model dataType="de.ugoe.cs.rwm.domain.shark.String" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Databasedependency!databasePassword'"
	 * @generated
	 */
	String getDatabasePassword();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabasePassword <em>Database Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database Password</em>' attribute.
	 * @see #getDatabasePassword()
	 * @generated
	 */
	void setDatabasePassword(String value);

	/**
	 * Returns the value of the '<em><b>Database Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Database Name</em>' attribute.
	 * @see #setDatabaseName(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency_DatabaseName()
	 * @model dataType="de.ugoe.cs.rwm.domain.shark.String" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Databasedependency!databaseName'"
	 * @generated
	 */
	String getDatabaseName();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseName <em>Database Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database Name</em>' attribute.
	 * @see #getDatabaseName()
	 * @generated
	 */
	void setDatabaseName(String value);

	/**
	 * Returns the value of the '<em><b>Database Hostname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Database Hostname</em>' attribute.
	 * @see #setDatabaseHostname(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency_DatabaseHostname()
	 * @model dataType="de.ugoe.cs.rwm.domain.shark.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Databasedependency!databaseHostname'"
	 * @generated
	 */
	String getDatabaseHostname();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseHostname <em>Database Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database Hostname</em>' attribute.
	 * @see #getDatabaseHostname()
	 * @generated
	 */
	void setDatabaseHostname(String value);

	/**
	 * Returns the value of the '<em><b>Database Port</b></em>' attribute.
	 * The default value is <code>"27017"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Database Port</em>' attribute.
	 * @see #setDatabasePort(Integer)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency_DatabasePort()
	 * @model default="27017" dataType="modmacao.Port"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Databasedependency!databasePort'"
	 * @generated
	 */
	Integer getDatabasePort();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabasePort <em>Database Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database Port</em>' attribute.
	 * @see #getDatabasePort()
	 * @generated
	 */
	void setDatabasePort(Integer value);

	/**
	 * Returns the value of the '<em><b>Database Authentication</b></em>' attribute.
	 * The default value is <code>"None"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Database Authentication</em>' attribute.
	 * @see #setDatabaseAuthentication(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency_DatabaseAuthentication()
	 * @model default="None" dataType="de.ugoe.cs.rwm.domain.shark.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Databasedependency!databaseAuthentication'"
	 * @generated
	 */
	String getDatabaseAuthentication();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseAuthentication <em>Database Authentication</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database Authentication</em>' attribute.
	 * @see #getDatabaseAuthentication()
	 * @generated
	 */
	void setDatabaseAuthentication(String value);

	/**
	 * Returns the value of the '<em><b>Database Ssl Connection</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Database Ssl Connection</em>' attribute.
	 * @see #setDatabaseSslConnection(Boolean)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency_DatabaseSslConnection()
	 * @model default="false" dataType="de.ugoe.cs.rwm.domain.shark.Boolean"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Databasedependency!databaseSslConnection'"
	 * @generated
	 */
	Boolean getDatabaseSslConnection();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDatabaseSslConnection <em>Database Ssl Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database Ssl Connection</em>' attribute.
	 * @see #getDatabaseSslConnection()
	 * @generated
	 */
	void setDatabaseSslConnection(Boolean value);

	/**
	 * Returns the value of the '<em><b>Db Driver</b></em>' attribute.
	 * The default value is <code>"mongo"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Db Driver</em>' attribute.
	 * @see #setDbDriver(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getDatabasedependency_DbDriver()
	 * @model default="mongo" dataType="de.ugoe.cs.rwm.domain.shark.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Databasedependency!dbDriver'"
	 * @generated
	 */
	String getDbDriver();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Databasedependency#getDbDriver <em>Db Driver</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Db Driver</em>' attribute.
	 * @see #getDbDriver()
	 * @generated
	 */
	void setDbDriver(String value);

} // Databasedependency
