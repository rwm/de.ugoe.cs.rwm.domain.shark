/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.util;

import de.ugoe.cs.rwm.domain.shark.*;

import modmacao.Component;
import modmacao.Dependency;
import modmacao.Executiondependency;

import org.eclipse.cmf.occi.core.MixinBase;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage
 * @generated
 */
public class SharkSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SharkPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SharkSwitch() {
		if (modelPackage == null) {
			modelPackage = SharkPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case SharkPackage.SHARKJOB: {
				Sharkjob sharkjob = (Sharkjob)theEObject;
				T result = caseSharkjob(sharkjob);
				if (result == null) result = caseComponent(sharkjob);
				if (result == null) result = caseMixinBase(sharkjob);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.VCSJOB: {
				Vcsjob vcsjob = (Vcsjob)theEObject;
				T result = caseVcsjob(vcsjob);
				if (result == null) result = caseSharkjob(vcsjob);
				if (result == null) result = caseComponent(vcsjob);
				if (result == null) result = caseMixinBase(vcsjob);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.MECOJOB: {
				Mecojob mecojob = (Mecojob)theEObject;
				T result = caseMecojob(mecojob);
				if (result == null) result = caseSharkjobrevision(mecojob);
				if (result == null) result = caseSharkjob(mecojob);
				if (result == null) result = caseComponent(mecojob);
				if (result == null) result = caseMixinBase(mecojob);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.COASTJOB: {
				Coastjob coastjob = (Coastjob)theEObject;
				T result = caseCoastjob(coastjob);
				if (result == null) result = caseSharkjobrevision(coastjob);
				if (result == null) result = caseSharkjob(coastjob);
				if (result == null) result = caseComponent(coastjob);
				if (result == null) result = caseMixinBase(coastjob);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.MEMEJOB: {
				Memejob memejob = (Memejob)theEObject;
				T result = caseMemejob(memejob);
				if (result == null) result = caseSharkjob(memejob);
				if (result == null) result = caseComponent(memejob);
				if (result == null) result = caseMixinBase(memejob);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.DATABASEDEPENDENCY: {
				Databasedependency databasedependency = (Databasedependency)theEObject;
				T result = caseDatabasedependency(databasedependency);
				if (result == null) result = caseExecutiondependency(databasedependency);
				if (result == null) result = caseDependency(databasedependency);
				if (result == null) result = caseMixinBase(databasedependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.SHARKJOBREVISION: {
				Sharkjobrevision sharkjobrevision = (Sharkjobrevision)theEObject;
				T result = caseSharkjobrevision(sharkjobrevision);
				if (result == null) result = caseSharkjob(sharkjobrevision);
				if (result == null) result = caseComponent(sharkjobrevision);
				if (result == null) result = caseMixinBase(sharkjobrevision);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.VCSSHARK: {
				Vcsshark vcsshark = (Vcsshark)theEObject;
				T result = caseVcsshark(vcsshark);
				if (result == null) result = caseComponent(vcsshark);
				if (result == null) result = caseMixinBase(vcsshark);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.MEMESHARK: {
				Memeshark memeshark = (Memeshark)theEObject;
				T result = caseMemeshark(memeshark);
				if (result == null) result = caseComponent(memeshark);
				if (result == null) result = caseMixinBase(memeshark);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.MECOSHARK: {
				Mecoshark mecoshark = (Mecoshark)theEObject;
				T result = caseMecoshark(mecoshark);
				if (result == null) result = caseComponent(mecoshark);
				if (result == null) result = caseMixinBase(mecoshark);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.COASTSHARK: {
				Coastshark coastshark = (Coastshark)theEObject;
				T result = caseCoastshark(coastshark);
				if (result == null) result = caseComponent(coastshark);
				if (result == null) result = caseMixinBase(coastshark);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.DATABASEQUERY: {
				Databasequery databasequery = (Databasequery)theEObject;
				T result = caseDatabasequery(databasequery);
				if (result == null) result = caseComponent(databasequery);
				if (result == null) result = caseMixinBase(databasequery);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SharkPackage.MONGOQUERY: {
				Mongoquery mongoquery = (Mongoquery)theEObject;
				T result = caseMongoquery(mongoquery);
				if (result == null) result = caseDatabasequery(mongoquery);
				if (result == null) result = caseComponent(mongoquery);
				if (result == null) result = caseMixinBase(mongoquery);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sharkjob</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sharkjob</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSharkjob(Sharkjob object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vcsjob</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vcsjob</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVcsjob(Vcsjob object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mecojob</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mecojob</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMecojob(Mecojob object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Coastjob</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Coastjob</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCoastjob(Coastjob object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Memejob</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Memejob</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMemejob(Memejob object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Databasedependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Databasedependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDatabasedependency(Databasedependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sharkjobrevision</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sharkjobrevision</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSharkjobrevision(Sharkjobrevision object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vcsshark</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vcsshark</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVcsshark(Vcsshark object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Memeshark</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Memeshark</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMemeshark(Memeshark object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mecoshark</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mecoshark</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMecoshark(Mecoshark object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Coastshark</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Coastshark</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCoastshark(Coastshark object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Databasequery</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Databasequery</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDatabasequery(Databasequery object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mongoquery</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mongoquery</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMongoquery(Mongoquery object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mixin Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mixin Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMixinBase(MixinBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponent(Component object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDependency(Dependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Executiondependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Executiondependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecutiondependency(Executiondependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SharkSwitch
