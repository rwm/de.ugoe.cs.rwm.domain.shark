/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mecojob</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Mecojob#getMecoOutputDirectory <em>Meco Output Directory</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getMecojob()
 * @model
 * @generated
 */
public interface Mecojob extends Sharkjobrevision, MixinBase {
	/**
	 * Returns the value of the '<em><b>Meco Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Meco Output Directory</em>' attribute.
	 * @see #setMecoOutputDirectory(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getMecojob_MecoOutputDirectory()
	 * @model dataType="de.ugoe.cs.rwm.domain.shark.String" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Mecojob!mecoOutputDirectory'"
	 * @generated
	 */
	String getMecoOutputDirectory();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Mecojob#getMecoOutputDirectory <em>Meco Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Meco Output Directory</em>' attribute.
	 * @see #getMecoOutputDirectory()
	 * @generated
	 */
	void setMecoOutputDirectory(String value);

} // Mecojob
