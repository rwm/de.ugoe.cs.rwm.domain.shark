/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark;

import java.net.URL;

import modmacao.Component;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sharkjob</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkLogLevel <em>Shark Log Level</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectRepository <em>Shark Project Repository</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectName <em>Shark Project Name</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectDirectory <em>Shark Project Directory</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getSharkjob()
 * @model
 * @generated
 */
public interface Sharkjob extends Component, MixinBase {
	/**
	 * Returns the value of the '<em><b>Shark Log Level</b></em>' attribute.
	 * The default value is <code>"INFO"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Shark Log Level</em>' attribute.
	 * @see #setSharkLogLevel(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getSharkjob_SharkLogLevel()
	 * @model default="INFO" dataType="de.ugoe.cs.rwm.domain.shark.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Sharkjob!sharkLogLevel'"
	 * @generated
	 */
	String getSharkLogLevel();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkLogLevel <em>Shark Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shark Log Level</em>' attribute.
	 * @see #getSharkLogLevel()
	 * @generated
	 */
	void setSharkLogLevel(String value);

	/**
	 * Returns the value of the '<em><b>Shark Project Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Shark Project Repository</em>' attribute.
	 * @see #setSharkProjectRepository(URL)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getSharkjob_SharkProjectRepository()
	 * @model dataType="org.modmacao.occi.platform.URL" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Sharkjob!sharkProjectRepository'"
	 * @generated
	 */
	URL getSharkProjectRepository();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectRepository <em>Shark Project Repository</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shark Project Repository</em>' attribute.
	 * @see #getSharkProjectRepository()
	 * @generated
	 */
	void setSharkProjectRepository(URL value);

	/**
	 * Returns the value of the '<em><b>Shark Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Shark Project Name</em>' attribute.
	 * @see #setSharkProjectName(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getSharkjob_SharkProjectName()
	 * @model dataType="de.ugoe.cs.rwm.domain.shark.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Sharkjob!sharkProjectName'"
	 * @generated
	 */
	String getSharkProjectName();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectName <em>Shark Project Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shark Project Name</em>' attribute.
	 * @see #getSharkProjectName()
	 * @generated
	 */
	void setSharkProjectName(String value);

	/**
	 * Returns the value of the '<em><b>Shark Project Directory</b></em>' attribute.
	 * The default value is <code>"\"\""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Shark Project Directory</em>' attribute.
	 * @see #setSharkProjectDirectory(String)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getSharkjob_SharkProjectDirectory()
	 * @model default="\"\"" dataType="de.ugoe.cs.rwm.domain.shark.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Sharkjob!sharkProjectDirectory'"
	 * @generated
	 */
	String getSharkProjectDirectory();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Sharkjob#getSharkProjectDirectory <em>Shark Project Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shark Project Directory</em>' attribute.
	 * @see #getSharkProjectDirectory()
	 * @generated
	 */
	void setSharkProjectDirectory(String value);

} // Sharkjob
