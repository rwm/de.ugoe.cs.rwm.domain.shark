/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vcsjob</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Vcsjob#getVcsUsedcores <em>Vcs Usedcores</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getVcsjob()
 * @model
 * @generated
 */
public interface Vcsjob extends Sharkjob, MixinBase {
	/**
	 * Returns the value of the '<em><b>Vcs Usedcores</b></em>' attribute.
	 * The default value is <code>"4"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Vcs Usedcores</em>' attribute.
	 * @see #setVcsUsedcores(Integer)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getVcsjob_VcsUsedcores()
	 * @model default="4" dataType="de.ugoe.cs.rwm.domain.shark.Integer"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Vcsjob!vcsUsedcores'"
	 * @generated
	 */
	Integer getVcsUsedcores();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Vcsjob#getVcsUsedcores <em>Vcs Usedcores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vcs Usedcores</em>' attribute.
	 * @see #getVcsUsedcores()
	 * @generated
	 */
	void setVcsUsedcores(Integer value);

} // Vcsjob
