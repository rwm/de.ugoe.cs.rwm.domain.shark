/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memejob</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.Memejob#getMemeParallelProcesses <em>Meme Parallel Processes</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getMemejob()
 * @model
 * @generated
 */
public interface Memejob extends Sharkjob, MixinBase {
	/**
	 * Returns the value of the '<em><b>Meme Parallel Processes</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Meme Parallel Processes</em>' attribute.
	 * @see #setMemeParallelProcesses(Integer)
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#getMemejob_MemeParallelProcesses()
	 * @model default="1" dataType="de.ugoe.cs.rwm.domain.shark.Integer"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/domain/shark/ecore!Memejob!memeParallelProcesses'"
	 * @generated
	 */
	Integer getMemeParallelProcesses();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.shark.Memejob#getMemeParallelProcesses <em>Meme Parallel Processes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Meme Parallel Processes</em>' attribute.
	 * @see #getMemeParallelProcesses()
	 * @generated
	 */
	void setMemeParallelProcesses(Integer value);

} // Memejob
