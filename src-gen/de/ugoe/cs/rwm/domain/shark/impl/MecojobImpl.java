/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.Mecojob;
import de.ugoe.cs.rwm.domain.shark.SharkPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mecojob</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.MecojobImpl#getMecoOutputDirectory <em>Meco Output Directory</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MecojobImpl extends SharkjobrevisionImpl implements Mecojob {
	/**
	 * The default value of the '{@link #getMecoOutputDirectory() <em>Meco Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMecoOutputDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final String MECO_OUTPUT_DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMecoOutputDirectory() <em>Meco Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMecoOutputDirectory()
	 * @generated
	 * @ordered
	 */
	protected String mecoOutputDirectory = MECO_OUTPUT_DIRECTORY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MecojobImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SharkPackage.Literals.MECOJOB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMecoOutputDirectory() {
		return mecoOutputDirectory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMecoOutputDirectory(String newMecoOutputDirectory) {
		String oldMecoOutputDirectory = mecoOutputDirectory;
		mecoOutputDirectory = newMecoOutputDirectory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.MECOJOB__MECO_OUTPUT_DIRECTORY, oldMecoOutputDirectory, mecoOutputDirectory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SharkPackage.MECOJOB__MECO_OUTPUT_DIRECTORY:
				return getMecoOutputDirectory();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SharkPackage.MECOJOB__MECO_OUTPUT_DIRECTORY:
				setMecoOutputDirectory((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SharkPackage.MECOJOB__MECO_OUTPUT_DIRECTORY:
				setMecoOutputDirectory(MECO_OUTPUT_DIRECTORY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SharkPackage.MECOJOB__MECO_OUTPUT_DIRECTORY:
				return MECO_OUTPUT_DIRECTORY_EDEFAULT == null ? mecoOutputDirectory != null : !MECO_OUTPUT_DIRECTORY_EDEFAULT.equals(mecoOutputDirectory);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mecoOutputDirectory: ");
		result.append(mecoOutputDirectory);
		result.append(')');
		return result.toString();
	}

} //MecojobImpl
