/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.SharkPackage;
import de.ugoe.cs.rwm.domain.shark.Sharkjob;

import java.net.URL;

import modmacao.impl.ComponentImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sharkjob</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobImpl#getSharkLogLevel <em>Shark Log Level</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobImpl#getSharkProjectRepository <em>Shark Project Repository</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobImpl#getSharkProjectName <em>Shark Project Name</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobImpl#getSharkProjectDirectory <em>Shark Project Directory</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SharkjobImpl extends ComponentImpl implements Sharkjob {
	/**
	 * The default value of the '{@link #getSharkLogLevel() <em>Shark Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkLogLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String SHARK_LOG_LEVEL_EDEFAULT = "INFO";

	/**
	 * The cached value of the '{@link #getSharkLogLevel() <em>Shark Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkLogLevel()
	 * @generated
	 * @ordered
	 */
	protected String sharkLogLevel = SHARK_LOG_LEVEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSharkProjectRepository() <em>Shark Project Repository</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkProjectRepository()
	 * @generated
	 * @ordered
	 */
	protected static final URL SHARK_PROJECT_REPOSITORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSharkProjectRepository() <em>Shark Project Repository</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkProjectRepository()
	 * @generated
	 * @ordered
	 */
	protected URL sharkProjectRepository = SHARK_PROJECT_REPOSITORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getSharkProjectName() <em>Shark Project Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkProjectName()
	 * @generated
	 * @ordered
	 */
	protected static final String SHARK_PROJECT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSharkProjectName() <em>Shark Project Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkProjectName()
	 * @generated
	 * @ordered
	 */
	protected String sharkProjectName = SHARK_PROJECT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSharkProjectDirectory() <em>Shark Project Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkProjectDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final String SHARK_PROJECT_DIRECTORY_EDEFAULT = "\"\"";

	/**
	 * The cached value of the '{@link #getSharkProjectDirectory() <em>Shark Project Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkProjectDirectory()
	 * @generated
	 * @ordered
	 */
	protected String sharkProjectDirectory = SHARK_PROJECT_DIRECTORY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SharkjobImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SharkPackage.Literals.SHARKJOB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSharkLogLevel() {
		return sharkLogLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSharkLogLevel(String newSharkLogLevel) {
		String oldSharkLogLevel = sharkLogLevel;
		sharkLogLevel = newSharkLogLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.SHARKJOB__SHARK_LOG_LEVEL, oldSharkLogLevel, sharkLogLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URL getSharkProjectRepository() {
		return sharkProjectRepository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSharkProjectRepository(URL newSharkProjectRepository) {
		URL oldSharkProjectRepository = sharkProjectRepository;
		sharkProjectRepository = newSharkProjectRepository;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.SHARKJOB__SHARK_PROJECT_REPOSITORY, oldSharkProjectRepository, sharkProjectRepository));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSharkProjectName() {
		return sharkProjectName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSharkProjectName(String newSharkProjectName) {
		String oldSharkProjectName = sharkProjectName;
		sharkProjectName = newSharkProjectName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.SHARKJOB__SHARK_PROJECT_NAME, oldSharkProjectName, sharkProjectName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSharkProjectDirectory() {
		return sharkProjectDirectory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSharkProjectDirectory(String newSharkProjectDirectory) {
		String oldSharkProjectDirectory = sharkProjectDirectory;
		sharkProjectDirectory = newSharkProjectDirectory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.SHARKJOB__SHARK_PROJECT_DIRECTORY, oldSharkProjectDirectory, sharkProjectDirectory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SharkPackage.SHARKJOB__SHARK_LOG_LEVEL:
				return getSharkLogLevel();
			case SharkPackage.SHARKJOB__SHARK_PROJECT_REPOSITORY:
				return getSharkProjectRepository();
			case SharkPackage.SHARKJOB__SHARK_PROJECT_NAME:
				return getSharkProjectName();
			case SharkPackage.SHARKJOB__SHARK_PROJECT_DIRECTORY:
				return getSharkProjectDirectory();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SharkPackage.SHARKJOB__SHARK_LOG_LEVEL:
				setSharkLogLevel((String)newValue);
				return;
			case SharkPackage.SHARKJOB__SHARK_PROJECT_REPOSITORY:
				setSharkProjectRepository((URL)newValue);
				return;
			case SharkPackage.SHARKJOB__SHARK_PROJECT_NAME:
				setSharkProjectName((String)newValue);
				return;
			case SharkPackage.SHARKJOB__SHARK_PROJECT_DIRECTORY:
				setSharkProjectDirectory((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SharkPackage.SHARKJOB__SHARK_LOG_LEVEL:
				setSharkLogLevel(SHARK_LOG_LEVEL_EDEFAULT);
				return;
			case SharkPackage.SHARKJOB__SHARK_PROJECT_REPOSITORY:
				setSharkProjectRepository(SHARK_PROJECT_REPOSITORY_EDEFAULT);
				return;
			case SharkPackage.SHARKJOB__SHARK_PROJECT_NAME:
				setSharkProjectName(SHARK_PROJECT_NAME_EDEFAULT);
				return;
			case SharkPackage.SHARKJOB__SHARK_PROJECT_DIRECTORY:
				setSharkProjectDirectory(SHARK_PROJECT_DIRECTORY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SharkPackage.SHARKJOB__SHARK_LOG_LEVEL:
				return SHARK_LOG_LEVEL_EDEFAULT == null ? sharkLogLevel != null : !SHARK_LOG_LEVEL_EDEFAULT.equals(sharkLogLevel);
			case SharkPackage.SHARKJOB__SHARK_PROJECT_REPOSITORY:
				return SHARK_PROJECT_REPOSITORY_EDEFAULT == null ? sharkProjectRepository != null : !SHARK_PROJECT_REPOSITORY_EDEFAULT.equals(sharkProjectRepository);
			case SharkPackage.SHARKJOB__SHARK_PROJECT_NAME:
				return SHARK_PROJECT_NAME_EDEFAULT == null ? sharkProjectName != null : !SHARK_PROJECT_NAME_EDEFAULT.equals(sharkProjectName);
			case SharkPackage.SHARKJOB__SHARK_PROJECT_DIRECTORY:
				return SHARK_PROJECT_DIRECTORY_EDEFAULT == null ? sharkProjectDirectory != null : !SHARK_PROJECT_DIRECTORY_EDEFAULT.equals(sharkProjectDirectory);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sharkLogLevel: ");
		result.append(sharkLogLevel);
		result.append(", sharkProjectRepository: ");
		result.append(sharkProjectRepository);
		result.append(", sharkProjectName: ");
		result.append(sharkProjectName);
		result.append(", sharkProjectDirectory: ");
		result.append(sharkProjectDirectory);
		result.append(')');
		return result.toString();
	}

} //SharkjobImpl
