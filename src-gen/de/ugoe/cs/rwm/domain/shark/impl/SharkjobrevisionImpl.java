/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.SharkPackage;
import de.ugoe.cs.rwm.domain.shark.Sharkjobrevision;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sharkjobrevision</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.SharkjobrevisionImpl#getSharkProjectRevision <em>Shark Project Revision</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SharkjobrevisionImpl extends SharkjobImpl implements Sharkjobrevision {
	/**
	 * The default value of the '{@link #getSharkProjectRevision() <em>Shark Project Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkProjectRevision()
	 * @generated
	 * @ordered
	 */
	protected static final String SHARK_PROJECT_REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSharkProjectRevision() <em>Shark Project Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharkProjectRevision()
	 * @generated
	 * @ordered
	 */
	protected String sharkProjectRevision = SHARK_PROJECT_REVISION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SharkjobrevisionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SharkPackage.Literals.SHARKJOBREVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSharkProjectRevision() {
		return sharkProjectRevision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSharkProjectRevision(String newSharkProjectRevision) {
		String oldSharkProjectRevision = sharkProjectRevision;
		sharkProjectRevision = newSharkProjectRevision;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.SHARKJOBREVISION__SHARK_PROJECT_REVISION, oldSharkProjectRevision, sharkProjectRevision));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SharkPackage.SHARKJOBREVISION__SHARK_PROJECT_REVISION:
				return getSharkProjectRevision();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SharkPackage.SHARKJOBREVISION__SHARK_PROJECT_REVISION:
				setSharkProjectRevision((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SharkPackage.SHARKJOBREVISION__SHARK_PROJECT_REVISION:
				setSharkProjectRevision(SHARK_PROJECT_REVISION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SharkPackage.SHARKJOBREVISION__SHARK_PROJECT_REVISION:
				return SHARK_PROJECT_REVISION_EDEFAULT == null ? sharkProjectRevision != null : !SHARK_PROJECT_REVISION_EDEFAULT.equals(sharkProjectRevision);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sharkProjectRevision: ");
		result.append(sharkProjectRevision);
		result.append(')');
		return result.toString();
	}

} //SharkjobrevisionImpl
