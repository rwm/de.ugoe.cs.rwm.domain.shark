/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SharkFactoryImpl extends EFactoryImpl implements SharkFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SharkFactory init() {
		try {
			SharkFactory theSharkFactory = (SharkFactory)EPackage.Registry.INSTANCE.getEFactory(SharkPackage.eNS_URI);
			if (theSharkFactory != null) {
				return theSharkFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SharkFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SharkFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SharkPackage.SHARKJOB: return createSharkjob();
			case SharkPackage.VCSJOB: return createVcsjob();
			case SharkPackage.MECOJOB: return createMecojob();
			case SharkPackage.COASTJOB: return createCoastjob();
			case SharkPackage.MEMEJOB: return createMemejob();
			case SharkPackage.DATABASEDEPENDENCY: return createDatabasedependency();
			case SharkPackage.SHARKJOBREVISION: return createSharkjobrevision();
			case SharkPackage.VCSSHARK: return createVcsshark();
			case SharkPackage.MEMESHARK: return createMemeshark();
			case SharkPackage.MECOSHARK: return createMecoshark();
			case SharkPackage.COASTSHARK: return createCoastshark();
			case SharkPackage.DATABASEQUERY: return createDatabasequery();
			case SharkPackage.MONGOQUERY: return createMongoquery();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case SharkPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case SharkPackage.BOOLEAN:
				return createBooleanFromString(eDataType, initialValue);
			case SharkPackage.INTEGER:
				return createIntegerFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case SharkPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case SharkPackage.BOOLEAN:
				return convertBooleanToString(eDataType, instanceValue);
			case SharkPackage.INTEGER:
				return convertIntegerToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Sharkjob createSharkjob() {
		SharkjobImpl sharkjob = new SharkjobImpl();
		return sharkjob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Vcsjob createVcsjob() {
		VcsjobImpl vcsjob = new VcsjobImpl();
		return vcsjob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Mecojob createMecojob() {
		MecojobImpl mecojob = new MecojobImpl();
		return mecojob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Coastjob createCoastjob() {
		CoastjobImpl coastjob = new CoastjobImpl();
		return coastjob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Memejob createMemejob() {
		MemejobImpl memejob = new MemejobImpl();
		return memejob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Databasedependency createDatabasedependency() {
		DatabasedependencyImpl databasedependency = new DatabasedependencyImpl();
		return databasedependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Sharkjobrevision createSharkjobrevision() {
		SharkjobrevisionImpl sharkjobrevision = new SharkjobrevisionImpl();
		return sharkjobrevision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Vcsshark createVcsshark() {
		VcssharkImpl vcsshark = new VcssharkImpl();
		return vcsshark;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Memeshark createMemeshark() {
		MemesharkImpl memeshark = new MemesharkImpl();
		return memeshark;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Mecoshark createMecoshark() {
		MecosharkImpl mecoshark = new MecosharkImpl();
		return mecoshark;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Coastshark createCoastshark() {
		CoastsharkImpl coastshark = new CoastsharkImpl();
		return coastshark;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Databasequery createDatabasequery() {
		DatabasequeryImpl databasequery = new DatabasequeryImpl();
		return databasequery;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Mongoquery createMongoquery() {
		MongoqueryImpl mongoquery = new MongoqueryImpl();
		return mongoquery;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean createBooleanFromString(EDataType eDataType, String initialValue) {
		return (Boolean)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBooleanToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createIntegerFromString(EDataType eDataType, String initialValue) {
		return (Integer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIntegerToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SharkPackage getSharkPackage() {
		return (SharkPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SharkPackage getPackage() {
		return SharkPackage.eINSTANCE;
	}

} //SharkFactoryImpl
