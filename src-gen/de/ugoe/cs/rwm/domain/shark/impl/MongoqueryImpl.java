/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.Mongoquery;
import de.ugoe.cs.rwm.domain.shark.SharkPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mongoquery</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.MongoqueryImpl#getMongoEvalJs <em>Mongo Eval Js</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MongoqueryImpl extends DatabasequeryImpl implements Mongoquery {
	/**
	 * The default value of the '{@link #getMongoEvalJs() <em>Mongo Eval Js</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMongoEvalJs()
	 * @generated
	 * @ordered
	 */
	protected static final String MONGO_EVAL_JS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMongoEvalJs() <em>Mongo Eval Js</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMongoEvalJs()
	 * @generated
	 * @ordered
	 */
	protected String mongoEvalJs = MONGO_EVAL_JS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MongoqueryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SharkPackage.Literals.MONGOQUERY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMongoEvalJs() {
		return mongoEvalJs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMongoEvalJs(String newMongoEvalJs) {
		String oldMongoEvalJs = mongoEvalJs;
		mongoEvalJs = newMongoEvalJs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.MONGOQUERY__MONGO_EVAL_JS, oldMongoEvalJs, mongoEvalJs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SharkPackage.MONGOQUERY__MONGO_EVAL_JS:
				return getMongoEvalJs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SharkPackage.MONGOQUERY__MONGO_EVAL_JS:
				setMongoEvalJs((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SharkPackage.MONGOQUERY__MONGO_EVAL_JS:
				setMongoEvalJs(MONGO_EVAL_JS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SharkPackage.MONGOQUERY__MONGO_EVAL_JS:
				return MONGO_EVAL_JS_EDEFAULT == null ? mongoEvalJs != null : !MONGO_EVAL_JS_EDEFAULT.equals(mongoEvalJs);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mongoEvalJs: ");
		result.append(mongoEvalJs);
		result.append(')');
		return result.toString();
	}

} //MongoqueryImpl
