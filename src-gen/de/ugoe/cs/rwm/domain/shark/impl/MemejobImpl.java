/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.Memejob;
import de.ugoe.cs.rwm.domain.shark.SharkPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Memejob</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.MemejobImpl#getMemeParallelProcesses <em>Meme Parallel Processes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MemejobImpl extends SharkjobImpl implements Memejob {
	/**
	 * The default value of the '{@link #getMemeParallelProcesses() <em>Meme Parallel Processes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemeParallelProcesses()
	 * @generated
	 * @ordered
	 */
	protected static final Integer MEME_PARALLEL_PROCESSES_EDEFAULT = new Integer(1);

	/**
	 * The cached value of the '{@link #getMemeParallelProcesses() <em>Meme Parallel Processes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemeParallelProcesses()
	 * @generated
	 * @ordered
	 */
	protected Integer memeParallelProcesses = MEME_PARALLEL_PROCESSES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MemejobImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SharkPackage.Literals.MEMEJOB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getMemeParallelProcesses() {
		return memeParallelProcesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMemeParallelProcesses(Integer newMemeParallelProcesses) {
		Integer oldMemeParallelProcesses = memeParallelProcesses;
		memeParallelProcesses = newMemeParallelProcesses;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.MEMEJOB__MEME_PARALLEL_PROCESSES, oldMemeParallelProcesses, memeParallelProcesses));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SharkPackage.MEMEJOB__MEME_PARALLEL_PROCESSES:
				return getMemeParallelProcesses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SharkPackage.MEMEJOB__MEME_PARALLEL_PROCESSES:
				setMemeParallelProcesses((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SharkPackage.MEMEJOB__MEME_PARALLEL_PROCESSES:
				setMemeParallelProcesses(MEME_PARALLEL_PROCESSES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SharkPackage.MEMEJOB__MEME_PARALLEL_PROCESSES:
				return MEME_PARALLEL_PROCESSES_EDEFAULT == null ? memeParallelProcesses != null : !MEME_PARALLEL_PROCESSES_EDEFAULT.equals(memeParallelProcesses);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (memeParallelProcesses: ");
		result.append(memeParallelProcesses);
		result.append(')');
		return result.toString();
	}

} //MemejobImpl
