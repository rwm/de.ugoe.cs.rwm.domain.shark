/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.Databasedependency;
import de.ugoe.cs.rwm.domain.shark.SharkPackage;

import modmacao.impl.ExecutiondependencyImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Databasedependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl#getDatabaseUser <em>Database User</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl#getDatabasePassword <em>Database Password</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl#getDatabaseName <em>Database Name</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl#getDatabaseHostname <em>Database Hostname</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl#getDatabasePort <em>Database Port</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl#getDatabaseAuthentication <em>Database Authentication</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl#getDatabaseSslConnection <em>Database Ssl Connection</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.DatabasedependencyImpl#getDbDriver <em>Db Driver</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DatabasedependencyImpl extends ExecutiondependencyImpl implements Databasedependency {
	/**
	 * The default value of the '{@link #getDatabaseUser() <em>Database User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseUser()
	 * @generated
	 * @ordered
	 */
	protected static final String DATABASE_USER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDatabaseUser() <em>Database User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseUser()
	 * @generated
	 * @ordered
	 */
	protected String databaseUser = DATABASE_USER_EDEFAULT;

	/**
	 * The default value of the '{@link #getDatabasePassword() <em>Database Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabasePassword()
	 * @generated
	 * @ordered
	 */
	protected static final String DATABASE_PASSWORD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDatabasePassword() <em>Database Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabasePassword()
	 * @generated
	 * @ordered
	 */
	protected String databasePassword = DATABASE_PASSWORD_EDEFAULT;

	/**
	 * The default value of the '{@link #getDatabaseName() <em>Database Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseName()
	 * @generated
	 * @ordered
	 */
	protected static final String DATABASE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDatabaseName() <em>Database Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseName()
	 * @generated
	 * @ordered
	 */
	protected String databaseName = DATABASE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDatabaseHostname() <em>Database Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseHostname()
	 * @generated
	 * @ordered
	 */
	protected static final String DATABASE_HOSTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDatabaseHostname() <em>Database Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseHostname()
	 * @generated
	 * @ordered
	 */
	protected String databaseHostname = DATABASE_HOSTNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDatabasePort() <em>Database Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabasePort()
	 * @generated
	 * @ordered
	 */
	protected static final Integer DATABASE_PORT_EDEFAULT = new Integer(27017);

	/**
	 * The cached value of the '{@link #getDatabasePort() <em>Database Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabasePort()
	 * @generated
	 * @ordered
	 */
	protected Integer databasePort = DATABASE_PORT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDatabaseAuthentication() <em>Database Authentication</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseAuthentication()
	 * @generated
	 * @ordered
	 */
	protected static final String DATABASE_AUTHENTICATION_EDEFAULT = "None";

	/**
	 * The cached value of the '{@link #getDatabaseAuthentication() <em>Database Authentication</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseAuthentication()
	 * @generated
	 * @ordered
	 */
	protected String databaseAuthentication = DATABASE_AUTHENTICATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDatabaseSslConnection() <em>Database Ssl Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseSslConnection()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean DATABASE_SSL_CONNECTION_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getDatabaseSslConnection() <em>Database Ssl Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseSslConnection()
	 * @generated
	 * @ordered
	 */
	protected Boolean databaseSslConnection = DATABASE_SSL_CONNECTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDbDriver() <em>Db Driver</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDbDriver()
	 * @generated
	 * @ordered
	 */
	protected static final String DB_DRIVER_EDEFAULT = "mongo";

	/**
	 * The cached value of the '{@link #getDbDriver() <em>Db Driver</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDbDriver()
	 * @generated
	 * @ordered
	 */
	protected String dbDriver = DB_DRIVER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DatabasedependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SharkPackage.Literals.DATABASEDEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDatabaseUser() {
		return databaseUser;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabaseUser(String newDatabaseUser) {
		String oldDatabaseUser = databaseUser;
		databaseUser = newDatabaseUser;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.DATABASEDEPENDENCY__DATABASE_USER, oldDatabaseUser, databaseUser));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDatabasePassword() {
		return databasePassword;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabasePassword(String newDatabasePassword) {
		String oldDatabasePassword = databasePassword;
		databasePassword = newDatabasePassword;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.DATABASEDEPENDENCY__DATABASE_PASSWORD, oldDatabasePassword, databasePassword));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDatabaseName() {
		return databaseName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabaseName(String newDatabaseName) {
		String oldDatabaseName = databaseName;
		databaseName = newDatabaseName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.DATABASEDEPENDENCY__DATABASE_NAME, oldDatabaseName, databaseName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDatabaseHostname() {
		return databaseHostname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabaseHostname(String newDatabaseHostname) {
		String oldDatabaseHostname = databaseHostname;
		databaseHostname = newDatabaseHostname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.DATABASEDEPENDENCY__DATABASE_HOSTNAME, oldDatabaseHostname, databaseHostname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getDatabasePort() {
		return databasePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabasePort(Integer newDatabasePort) {
		Integer oldDatabasePort = databasePort;
		databasePort = newDatabasePort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.DATABASEDEPENDENCY__DATABASE_PORT, oldDatabasePort, databasePort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDatabaseAuthentication() {
		return databaseAuthentication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabaseAuthentication(String newDatabaseAuthentication) {
		String oldDatabaseAuthentication = databaseAuthentication;
		databaseAuthentication = newDatabaseAuthentication;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.DATABASEDEPENDENCY__DATABASE_AUTHENTICATION, oldDatabaseAuthentication, databaseAuthentication));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Boolean getDatabaseSslConnection() {
		return databaseSslConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabaseSslConnection(Boolean newDatabaseSslConnection) {
		Boolean oldDatabaseSslConnection = databaseSslConnection;
		databaseSslConnection = newDatabaseSslConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION, oldDatabaseSslConnection, databaseSslConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDbDriver() {
		return dbDriver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDbDriver(String newDbDriver) {
		String oldDbDriver = dbDriver;
		dbDriver = newDbDriver;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.DATABASEDEPENDENCY__DB_DRIVER, oldDbDriver, dbDriver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_USER:
				return getDatabaseUser();
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_PASSWORD:
				return getDatabasePassword();
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_NAME:
				return getDatabaseName();
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_HOSTNAME:
				return getDatabaseHostname();
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_PORT:
				return getDatabasePort();
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_AUTHENTICATION:
				return getDatabaseAuthentication();
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION:
				return getDatabaseSslConnection();
			case SharkPackage.DATABASEDEPENDENCY__DB_DRIVER:
				return getDbDriver();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_USER:
				setDatabaseUser((String)newValue);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_PASSWORD:
				setDatabasePassword((String)newValue);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_NAME:
				setDatabaseName((String)newValue);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_HOSTNAME:
				setDatabaseHostname((String)newValue);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_PORT:
				setDatabasePort((Integer)newValue);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_AUTHENTICATION:
				setDatabaseAuthentication((String)newValue);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION:
				setDatabaseSslConnection((Boolean)newValue);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DB_DRIVER:
				setDbDriver((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_USER:
				setDatabaseUser(DATABASE_USER_EDEFAULT);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_PASSWORD:
				setDatabasePassword(DATABASE_PASSWORD_EDEFAULT);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_NAME:
				setDatabaseName(DATABASE_NAME_EDEFAULT);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_HOSTNAME:
				setDatabaseHostname(DATABASE_HOSTNAME_EDEFAULT);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_PORT:
				setDatabasePort(DATABASE_PORT_EDEFAULT);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_AUTHENTICATION:
				setDatabaseAuthentication(DATABASE_AUTHENTICATION_EDEFAULT);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION:
				setDatabaseSslConnection(DATABASE_SSL_CONNECTION_EDEFAULT);
				return;
			case SharkPackage.DATABASEDEPENDENCY__DB_DRIVER:
				setDbDriver(DB_DRIVER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_USER:
				return DATABASE_USER_EDEFAULT == null ? databaseUser != null : !DATABASE_USER_EDEFAULT.equals(databaseUser);
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_PASSWORD:
				return DATABASE_PASSWORD_EDEFAULT == null ? databasePassword != null : !DATABASE_PASSWORD_EDEFAULT.equals(databasePassword);
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_NAME:
				return DATABASE_NAME_EDEFAULT == null ? databaseName != null : !DATABASE_NAME_EDEFAULT.equals(databaseName);
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_HOSTNAME:
				return DATABASE_HOSTNAME_EDEFAULT == null ? databaseHostname != null : !DATABASE_HOSTNAME_EDEFAULT.equals(databaseHostname);
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_PORT:
				return DATABASE_PORT_EDEFAULT == null ? databasePort != null : !DATABASE_PORT_EDEFAULT.equals(databasePort);
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_AUTHENTICATION:
				return DATABASE_AUTHENTICATION_EDEFAULT == null ? databaseAuthentication != null : !DATABASE_AUTHENTICATION_EDEFAULT.equals(databaseAuthentication);
			case SharkPackage.DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION:
				return DATABASE_SSL_CONNECTION_EDEFAULT == null ? databaseSslConnection != null : !DATABASE_SSL_CONNECTION_EDEFAULT.equals(databaseSslConnection);
			case SharkPackage.DATABASEDEPENDENCY__DB_DRIVER:
				return DB_DRIVER_EDEFAULT == null ? dbDriver != null : !DB_DRIVER_EDEFAULT.equals(dbDriver);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (databaseUser: ");
		result.append(databaseUser);
		result.append(", databasePassword: ");
		result.append(databasePassword);
		result.append(", databaseName: ");
		result.append(databaseName);
		result.append(", databaseHostname: ");
		result.append(databaseHostname);
		result.append(", databasePort: ");
		result.append(databasePort);
		result.append(", databaseAuthentication: ");
		result.append(databaseAuthentication);
		result.append(", databaseSslConnection: ");
		result.append(databaseSslConnection);
		result.append(", dbDriver: ");
		result.append(dbDriver);
		result.append(')');
		return result.toString();
	}

} //DatabasedependencyImpl
