/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.SharkPackage;
import de.ugoe.cs.rwm.domain.shark.Vcsjob;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vcsjob</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.VcsjobImpl#getVcsUsedcores <em>Vcs Usedcores</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VcsjobImpl extends SharkjobImpl implements Vcsjob {
	/**
	 * The default value of the '{@link #getVcsUsedcores() <em>Vcs Usedcores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVcsUsedcores()
	 * @generated
	 * @ordered
	 */
	protected static final Integer VCS_USEDCORES_EDEFAULT = new Integer(4);

	/**
	 * The cached value of the '{@link #getVcsUsedcores() <em>Vcs Usedcores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVcsUsedcores()
	 * @generated
	 * @ordered
	 */
	protected Integer vcsUsedcores = VCS_USEDCORES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VcsjobImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SharkPackage.Literals.VCSJOB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getVcsUsedcores() {
		return vcsUsedcores;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVcsUsedcores(Integer newVcsUsedcores) {
		Integer oldVcsUsedcores = vcsUsedcores;
		vcsUsedcores = newVcsUsedcores;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.VCSJOB__VCS_USEDCORES, oldVcsUsedcores, vcsUsedcores));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SharkPackage.VCSJOB__VCS_USEDCORES:
				return getVcsUsedcores();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SharkPackage.VCSJOB__VCS_USEDCORES:
				setVcsUsedcores((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SharkPackage.VCSJOB__VCS_USEDCORES:
				setVcsUsedcores(VCS_USEDCORES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SharkPackage.VCSJOB__VCS_USEDCORES:
				return VCS_USEDCORES_EDEFAULT == null ? vcsUsedcores != null : !VCS_USEDCORES_EDEFAULT.equals(vcsUsedcores);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (vcsUsedcores: ");
		result.append(vcsUsedcores);
		result.append(')');
		return result.toString();
	}

} //VcsjobImpl
