/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.Coastjob;
import de.ugoe.cs.rwm.domain.shark.SharkPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Coastjob</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.shark.impl.CoastjobImpl#getCoastMethodMetrics <em>Coast Method Metrics</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CoastjobImpl extends SharkjobrevisionImpl implements Coastjob {
	/**
	 * The default value of the '{@link #getCoastMethodMetrics() <em>Coast Method Metrics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoastMethodMetrics()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean COAST_METHOD_METRICS_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getCoastMethodMetrics() <em>Coast Method Metrics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoastMethodMetrics()
	 * @generated
	 * @ordered
	 */
	protected Boolean coastMethodMetrics = COAST_METHOD_METRICS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoastjobImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SharkPackage.Literals.COASTJOB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Boolean getCoastMethodMetrics() {
		return coastMethodMetrics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCoastMethodMetrics(Boolean newCoastMethodMetrics) {
		Boolean oldCoastMethodMetrics = coastMethodMetrics;
		coastMethodMetrics = newCoastMethodMetrics;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SharkPackage.COASTJOB__COAST_METHOD_METRICS, oldCoastMethodMetrics, coastMethodMetrics));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SharkPackage.COASTJOB__COAST_METHOD_METRICS:
				return getCoastMethodMetrics();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SharkPackage.COASTJOB__COAST_METHOD_METRICS:
				setCoastMethodMetrics((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SharkPackage.COASTJOB__COAST_METHOD_METRICS:
				setCoastMethodMetrics(COAST_METHOD_METRICS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SharkPackage.COASTJOB__COAST_METHOD_METRICS:
				return COAST_METHOD_METRICS_EDEFAULT == null ? coastMethodMetrics != null : !COAST_METHOD_METRICS_EDEFAULT.equals(coastMethodMetrics);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (coastMethodMetrics: ");
		result.append(coastMethodMetrics);
		result.append(')');
		return result.toString();
	}

} //CoastjobImpl
