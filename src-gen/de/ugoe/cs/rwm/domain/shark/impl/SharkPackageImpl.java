/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.shark.impl;

import de.ugoe.cs.rwm.domain.shark.Coastjob;
import de.ugoe.cs.rwm.domain.shark.Coastshark;
import de.ugoe.cs.rwm.domain.shark.Databasedependency;
import de.ugoe.cs.rwm.domain.shark.Databasequery;
import de.ugoe.cs.rwm.domain.shark.Mecojob;
import de.ugoe.cs.rwm.domain.shark.Mecoshark;
import de.ugoe.cs.rwm.domain.shark.Memejob;
import de.ugoe.cs.rwm.domain.shark.Memeshark;
import de.ugoe.cs.rwm.domain.shark.Mongoquery;
import de.ugoe.cs.rwm.domain.shark.SharkFactory;
import de.ugoe.cs.rwm.domain.shark.SharkPackage;
import de.ugoe.cs.rwm.domain.shark.Sharkjob;
import de.ugoe.cs.rwm.domain.shark.Sharkjobrevision;
import de.ugoe.cs.rwm.domain.shark.Vcsjob;
import de.ugoe.cs.rwm.domain.shark.Vcsshark;

import modmacao.ModmacaoPackage;

import org.eclipse.cmf.occi.core.OCCIPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.modmacao.occi.platform.PlatformPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SharkPackageImpl extends EPackageImpl implements SharkPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sharkjobEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vcsjobEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mecojobEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coastjobEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass memejobEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass databasedependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sharkjobrevisionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vcssharkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass memesharkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mecosharkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coastsharkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass databasequeryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mongoqueryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType booleanEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType integerEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.ugoe.cs.rwm.domain.shark.SharkPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SharkPackageImpl() {
		super(eNS_URI, SharkFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link SharkPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SharkPackage init() {
		if (isInited) return (SharkPackage)EPackage.Registry.INSTANCE.getEPackage(SharkPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredSharkPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		SharkPackageImpl theSharkPackage = registeredSharkPackage instanceof SharkPackageImpl ? (SharkPackageImpl)registeredSharkPackage : new SharkPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		ModmacaoPackage.eINSTANCE.eClass();
		PlatformPackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSharkPackage.createPackageContents();

		// Initialize created meta-data
		theSharkPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSharkPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SharkPackage.eNS_URI, theSharkPackage);
		return theSharkPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSharkjob() {
		return sharkjobEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSharkjob_SharkLogLevel() {
		return (EAttribute)sharkjobEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSharkjob_SharkProjectRepository() {
		return (EAttribute)sharkjobEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSharkjob_SharkProjectName() {
		return (EAttribute)sharkjobEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSharkjob_SharkProjectDirectory() {
		return (EAttribute)sharkjobEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVcsjob() {
		return vcsjobEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVcsjob_VcsUsedcores() {
		return (EAttribute)vcsjobEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMecojob() {
		return mecojobEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMecojob_MecoOutputDirectory() {
		return (EAttribute)mecojobEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCoastjob() {
		return coastjobEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCoastjob_CoastMethodMetrics() {
		return (EAttribute)coastjobEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMemejob() {
		return memejobEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMemejob_MemeParallelProcesses() {
		return (EAttribute)memejobEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDatabasedependency() {
		return databasedependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasedependency_DatabaseUser() {
		return (EAttribute)databasedependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasedependency_DatabasePassword() {
		return (EAttribute)databasedependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasedependency_DatabaseName() {
		return (EAttribute)databasedependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasedependency_DatabaseHostname() {
		return (EAttribute)databasedependencyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasedependency_DatabasePort() {
		return (EAttribute)databasedependencyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasedependency_DatabaseAuthentication() {
		return (EAttribute)databasedependencyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasedependency_DatabaseSslConnection() {
		return (EAttribute)databasedependencyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasedependency_DbDriver() {
		return (EAttribute)databasedependencyEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSharkjobrevision() {
		return sharkjobrevisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSharkjobrevision_SharkProjectRevision() {
		return (EAttribute)sharkjobrevisionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVcsshark() {
		return vcssharkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMemeshark() {
		return memesharkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMecoshark() {
		return mecosharkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCoastshark() {
		return coastsharkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDatabasequery() {
		return databasequeryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDatabasequery_DatabaseName() {
		return (EAttribute)databasequeryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMongoquery() {
		return mongoqueryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMongoquery_MongoEvalJs() {
		return (EAttribute)mongoqueryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getBoolean() {
		return booleanEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getInteger() {
		return integerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SharkFactory getSharkFactory() {
		return (SharkFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		sharkjobEClass = createEClass(SHARKJOB);
		createEAttribute(sharkjobEClass, SHARKJOB__SHARK_LOG_LEVEL);
		createEAttribute(sharkjobEClass, SHARKJOB__SHARK_PROJECT_REPOSITORY);
		createEAttribute(sharkjobEClass, SHARKJOB__SHARK_PROJECT_NAME);
		createEAttribute(sharkjobEClass, SHARKJOB__SHARK_PROJECT_DIRECTORY);

		vcsjobEClass = createEClass(VCSJOB);
		createEAttribute(vcsjobEClass, VCSJOB__VCS_USEDCORES);

		mecojobEClass = createEClass(MECOJOB);
		createEAttribute(mecojobEClass, MECOJOB__MECO_OUTPUT_DIRECTORY);

		coastjobEClass = createEClass(COASTJOB);
		createEAttribute(coastjobEClass, COASTJOB__COAST_METHOD_METRICS);

		memejobEClass = createEClass(MEMEJOB);
		createEAttribute(memejobEClass, MEMEJOB__MEME_PARALLEL_PROCESSES);

		databasedependencyEClass = createEClass(DATABASEDEPENDENCY);
		createEAttribute(databasedependencyEClass, DATABASEDEPENDENCY__DATABASE_USER);
		createEAttribute(databasedependencyEClass, DATABASEDEPENDENCY__DATABASE_PASSWORD);
		createEAttribute(databasedependencyEClass, DATABASEDEPENDENCY__DATABASE_NAME);
		createEAttribute(databasedependencyEClass, DATABASEDEPENDENCY__DATABASE_HOSTNAME);
		createEAttribute(databasedependencyEClass, DATABASEDEPENDENCY__DATABASE_PORT);
		createEAttribute(databasedependencyEClass, DATABASEDEPENDENCY__DATABASE_AUTHENTICATION);
		createEAttribute(databasedependencyEClass, DATABASEDEPENDENCY__DATABASE_SSL_CONNECTION);
		createEAttribute(databasedependencyEClass, DATABASEDEPENDENCY__DB_DRIVER);

		sharkjobrevisionEClass = createEClass(SHARKJOBREVISION);
		createEAttribute(sharkjobrevisionEClass, SHARKJOBREVISION__SHARK_PROJECT_REVISION);

		vcssharkEClass = createEClass(VCSSHARK);

		memesharkEClass = createEClass(MEMESHARK);

		mecosharkEClass = createEClass(MECOSHARK);

		coastsharkEClass = createEClass(COASTSHARK);

		databasequeryEClass = createEClass(DATABASEQUERY);
		createEAttribute(databasequeryEClass, DATABASEQUERY__DATABASE_NAME);

		mongoqueryEClass = createEClass(MONGOQUERY);
		createEAttribute(mongoqueryEClass, MONGOQUERY__MONGO_EVAL_JS);

		// Create data types
		stringEDataType = createEDataType(STRING);
		booleanEDataType = createEDataType(BOOLEAN);
		integerEDataType = createEDataType(INTEGER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ModmacaoPackage theModmacaoPackage = (ModmacaoPackage)EPackage.Registry.INSTANCE.getEPackage(ModmacaoPackage.eNS_URI);
		OCCIPackage theOCCIPackage = (OCCIPackage)EPackage.Registry.INSTANCE.getEPackage(OCCIPackage.eNS_URI);
		PlatformPackage thePlatformPackage = (PlatformPackage)EPackage.Registry.INSTANCE.getEPackage(PlatformPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		sharkjobEClass.getESuperTypes().add(theModmacaoPackage.getComponent());
		sharkjobEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		vcsjobEClass.getESuperTypes().add(this.getSharkjob());
		vcsjobEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		mecojobEClass.getESuperTypes().add(this.getSharkjobrevision());
		mecojobEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		coastjobEClass.getESuperTypes().add(this.getSharkjobrevision());
		coastjobEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		memejobEClass.getESuperTypes().add(this.getSharkjob());
		memejobEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		databasedependencyEClass.getESuperTypes().add(theModmacaoPackage.getExecutiondependency());
		databasedependencyEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		sharkjobrevisionEClass.getESuperTypes().add(this.getSharkjob());
		sharkjobrevisionEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		vcssharkEClass.getESuperTypes().add(theModmacaoPackage.getComponent());
		vcssharkEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		memesharkEClass.getESuperTypes().add(theModmacaoPackage.getComponent());
		memesharkEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		mecosharkEClass.getESuperTypes().add(theModmacaoPackage.getComponent());
		mecosharkEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		coastsharkEClass.getESuperTypes().add(theModmacaoPackage.getComponent());
		coastsharkEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		databasequeryEClass.getESuperTypes().add(theModmacaoPackage.getComponent());
		databasequeryEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		mongoqueryEClass.getESuperTypes().add(this.getDatabasequery());
		mongoqueryEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());

		// Initialize classes, features, and operations; add parameters
		initEClass(sharkjobEClass, Sharkjob.class, "Sharkjob", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSharkjob_SharkLogLevel(), this.getString(), "sharkLogLevel", "INFO", 0, 1, Sharkjob.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSharkjob_SharkProjectRepository(), thePlatformPackage.getURL(), "sharkProjectRepository", null, 1, 1, Sharkjob.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSharkjob_SharkProjectName(), this.getString(), "sharkProjectName", null, 0, 1, Sharkjob.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSharkjob_SharkProjectDirectory(), this.getString(), "sharkProjectDirectory", "\"\"", 0, 1, Sharkjob.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vcsjobEClass, Vcsjob.class, "Vcsjob", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVcsjob_VcsUsedcores(), this.getInteger(), "vcsUsedcores", "4", 0, 1, Vcsjob.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mecojobEClass, Mecojob.class, "Mecojob", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMecojob_MecoOutputDirectory(), this.getString(), "mecoOutputDirectory", null, 1, 1, Mecojob.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(coastjobEClass, Coastjob.class, "Coastjob", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCoastjob_CoastMethodMetrics(), this.getBoolean(), "coastMethodMetrics", "false", 0, 1, Coastjob.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(memejobEClass, Memejob.class, "Memejob", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMemejob_MemeParallelProcesses(), this.getInteger(), "memeParallelProcesses", "1", 0, 1, Memejob.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(databasedependencyEClass, Databasedependency.class, "Databasedependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDatabasedependency_DatabaseUser(), this.getString(), "databaseUser", null, 1, 1, Databasedependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabasedependency_DatabasePassword(), this.getString(), "databasePassword", null, 1, 1, Databasedependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabasedependency_DatabaseName(), this.getString(), "databaseName", null, 1, 1, Databasedependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabasedependency_DatabaseHostname(), this.getString(), "databaseHostname", null, 0, 1, Databasedependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabasedependency_DatabasePort(), theModmacaoPackage.getPort(), "databasePort", "27017", 0, 1, Databasedependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabasedependency_DatabaseAuthentication(), this.getString(), "databaseAuthentication", "None", 0, 1, Databasedependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabasedependency_DatabaseSslConnection(), this.getBoolean(), "databaseSslConnection", "false", 0, 1, Databasedependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabasedependency_DbDriver(), this.getString(), "dbDriver", "mongo", 0, 1, Databasedependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sharkjobrevisionEClass, Sharkjobrevision.class, "Sharkjobrevision", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSharkjobrevision_SharkProjectRevision(), this.getString(), "sharkProjectRevision", null, 1, 1, Sharkjobrevision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vcssharkEClass, Vcsshark.class, "Vcsshark", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(memesharkEClass, Memeshark.class, "Memeshark", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mecosharkEClass, Mecoshark.class, "Mecoshark", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(coastsharkEClass, Coastshark.class, "Coastshark", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(databasequeryEClass, Databasequery.class, "Databasequery", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDatabasequery_DatabaseName(), this.getString(), "databaseName", null, 0, 1, Databasequery.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mongoqueryEClass, Mongoquery.class, "Mongoquery", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMongoquery_MongoEvalJs(), this.getString(), "mongoEvalJs", null, 1, 1, Mongoquery.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(booleanEDataType, Boolean.class, "Boolean", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(integerEDataType, Integer.class, "Integer", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation
		  (this,
		   source,
		   new String[] {
		   });
	}

} //SharkPackageImpl
